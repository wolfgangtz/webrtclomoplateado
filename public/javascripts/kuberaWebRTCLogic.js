var peer; // Donde se guarda la instancia de la conexion con peer
var videoStreaming; // Guardamos el streaming de video local.
var audioStreaming; // Guardamos el streaming de audio local.
var sharingScreenVideoTag;
var videoMediaConnectionOtherPeer;
var audioMediaConnectionOtherPeer;
var videoStreamingOtherPeer;
var audioStreamingOtherPeer;
var dataConnectionOtherPeer;
var videoSharingScreenStreamingOtherPeer;
var videoStatus = false;
var audioStatus = false;
var peerServerConnection = false;
var otherPeerConnection = false;
var triyingVideoConnectionFlag = 0;
var triyingVideoSharingScreenConnectionFlag = 0;
var triyingAudioConnectionFlag = 0;
var audioConnectInitFlag = 0;
var videoConnectInitFlag = 0;
var audioInterval, videoInterval, sharingScreenInterval, monitoringIntervalVideo, monitoringIntervalAudio, monitoringIntervalSharingScreen;
var monitoringIntervalFlagVideo = 0;
var monitoringIntervalFlagAudio = 0;
var monitoringIntervalFlagSharingScreen = 0;
var whiteBoardInterval;
var sharingScreenStreaming;
var onSharingScreen = false;
var videoWidth = 320;
var videoHeigth = 240;
var qualitySelected = "QVGA";
var userLoguedId = 'user1';
var userOtherPeerId = 'user2';
var desk = undefined;
var slider;
var peerDestruido = 0;
var dataChannelMessagePendient = [];
var dataChannelInterval;
/*CONFIGURATION VAR*/
var conferenceDuration;
var totalConferenceDuration;
var meetingId;
var peerConfigKey;
var peerConfigPath;
var peerConfigPort;
var peerConfigUrl;
var redirectUrl;
var myRol;
var myName;
var roomChatName;
var openfireUserName;
var openfirePassword;
var preNotificationTime_client_8 = 4000;
var postNotificationTime_client_8 = 6000;
var preNotificationTime_client_12 = 4000;
var postNotificationTime_client_12 = 6000;
var preNotificationTime_consultor = 4000;
var postNotificationTime_consultor = 6000;
var preNotificationInterval;
var postNotificationInterval;
var sesionInit = false;
var sessionFinishedFlag = false;
var errorFound = 0;
var sharingScreenOpen = false;
var slidesOpen = false;
var chatOpen = false;
var whiteBoardOpen = false;
var chatNewMessages = 0;
var timeElapsedSeconds = 0;
var timeElapsedMinuts = 0;
var timeToReportSessionFinishedWhenLostServerConnection = 4000;
var exitFlag = true;
var reloadDFlag = 50;
var startClock = 0;
$(document).ready(function() {
	console.log(config);
	preNotificationTime_client_8 = config.timers.PRENOT_T_CLIENTE_8 * 1000;
	postNotificationTime_client_8 = config.timers.POSNOT_T_CLIENTE_8 * 1000;
	preNotificationTime_client_12 = config.timers.PRENOT_T_CLIENTE_12 * 1000;
	postNotificationTime_client_12 = config.timers.POSNOT_T_CLIENTE_12 * 1000;
	preNotificationTime_consultor = config.timers.PRENOT_T_CONSULTOR_13 * 1000;
	postNotificationTime_consultor = config.timers.POSNOT_T_CONSULTOR_13 * 1000;
	startClock = config.timers.START_CLOCK;
	roomChatName = config.roomName;
	myName = config.userName;
	openfireUserName = config.user_op;
	openfirePassword = config.pw_op;
	myRol = config.rol;
	redirectUrl = config.redirectURL;
	peerConfigKey = config.peer_key;
	peerConfigPath = config.peer_path;
	peerConfigPort = config.peer_port;
	peerConfigUrl = config.peer_url;
	userLoguedId = config.idPeer;
	userOtherPeerId = config.otherUser;
	conferenceDuration = config.duration;
	conferenceDurationInmuted = config.duration;
	totalConferenceDuration = config.initialDuration;
	meetingId = config.meetingID;
	if (myRol == "moderator") {
		myRol = "consultor";
	} else if (myRol == "attendee") {
		myRol = "cliente";
	}
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	if (util.browser != 'Unsupported' && util.supports.audioVideo) {
//console.log("NAVEGADOR COMPATIBLE Y NAVEGAODR CAPAZ DE TRANSMITIR AUDIO Y VDEO");
/*Creacion de peer*/
/**/
if (util.browser === 'Firefox') {
	peer = new Peer(userLoguedId, {
		key: peerConfigKey,
		host: peerConfigUrl,
		path: peerConfigPath,
		port: peerConfigPort,
		config: {
			"iceServers": [{
				"url": "stun:w2.xirsys.com"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:80?transport=udp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:3478?transport=udp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:80?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:3478?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turns:w2.xirsys.com:443?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turns:w2.xirsys.com:5349?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}]
		},
		debug: 3,
secure: true //indica si la conexon es con http (false) o https (true)
});
} else {
	peer = new Peer(userLoguedId, {
		key: peerConfigKey,
		host: peerConfigUrl,
		path: peerConfigPath,
		port: peerConfigPort,
		config: {
			"iceServers": [{
				"url": "stun:w2.xirsys.com"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:80?transport=udp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:3478?transport=udp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:80?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turn:w2.xirsys.com:3478?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turns:w2.xirsys.com:443?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}, {
				"username": "209b1c90-6b34-11e7-a0d2-a25dab79a36b",
				"url": "turns:w2.xirsys.com:5349?transport=tcp",
				"credential": "209b1d62-6b34-11e7-a26a-7255fe939ad1"
			}]
		},
		debug: 3,
secure: true //indica si la conexon es con http (false) o https (true)
});
}
/*Eventos de peer*/
/**/
peer.on('open', function(id) {
//console.log("CONEXION CON PEER SERVER EXITOSA BAJO EL IDENTIFICADOR --> " + id);
peerServerConnection = true;
var raphael = document.createElement("script");
raphael.type = "text/javascript";
raphael.src = urlKubera + "javascripts/libs-js/raphael.js";
$("#necesaryScripts").append(raphael);
var sharingScreen = document.createElement("script");
sharingScreen.type = "text/javascript";
sharingScreen.src = urlKubera + "javascripts/libs-js/getScreenId.js";
$("#necesaryScripts").append(sharingScreen);
var sliders = document.createElement("script");
sliders.type = "text/javascript";
sliders.src = urlKubera + "javascripts/libs-js/sliderUtils.js";
$("#necesaryScripts").append(sliders);
var networkMeasure = document.createElement("script");
networkMeasure.type = "text/javascript";
networkMeasure.src = urlKubera + "javascripts/libs-js/network.js";
$("#necesaryScripts").append(networkMeasure);
//Reportamos a kubera que EL CONSULTOR/CLIENTE PUDO ENTRAR A LA SALA
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_conectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR/CLIENTE PUDO ENTRAR A LA SALA");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR/CLIENTE PUDO ENTRAR A LA SALA
if (myRol == "consultor") {
	notificationLogic(preNotificationTime_client_8, postNotificationTime_client_8, 8);
	$('#text-conection').text(language.waitingClient);
} else if (myRol == "cliente") {
	notificationLogic(preNotificationTime_consultor, postNotificationTime_consultor, null);
	$('#text-conection').text(language.waitingConsultor);
}
});
peer.on('close', function() {
	if (peerDestruido == 0) {
		peerDestruido = 1;
//console.log("PEER DESTRUIDO");
peerServerConnection = false;
peer.destroy();
}
});
peer.on('disconnected', function() {
//console.log("CONEXION CON PEER SERVER PERDIDA");
//console.log("BREACKPOINT 1");
peerServerConnection = false;
if (peerDestruido == 0 && errorFound == 0) {
	peer.reconnect();
	if (myRol == "consultor") {
		$('#text-conection').text(language.waitingClient);
	} else if (myRol == "cliente") {
		$('#text-conection').text(language.waitingConsultor);
	}
} else {
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
}
});
peer.on('error', function(err) {
	if (exitFlag) {
		var msgNotConnect = "Could not connect to peer" + " " + userOtherPeerId;
		var errorFailed = "Failed to set local offer sdp: Called in wrong state: STATE_RECEIVEDOFFER";

		console.log("ERROR --> " + err.message);
//console.log("BREACKPOINT 2");
//console.log("ocurrio un error " + err.message + " ---> " + msgNotConnect);
if (errorFound == 0 && (msgNotConnect != err.message) && errorFailed != err.message) {
	peer.destroy();
	clearInterval(dataChannelInterval);
	exitFlag = false;
	errorFound = 1;
	var idErrorFound = 0;
	var flagNotRedirectoOtherSide = true;
	var RegexRepeatedId = "ID [\\s\\S]+ is taken";
	var regex = new RegExp(RegexRepeatedId);
//console.log("BREACKPOINT 3");
if (regex.test(String(err.message))) {
//console.log("BREACKPOINT 4");
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
idErrorFound = 1;
$('#text-conection').text(language.repeatId);
swal({
	title: language.errTitle,
	text: language.repeatId,
	timer: 6000,
	showConfirmButton: false
}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
//console.log("URL TO REDIRECT -> https://www.kubera.co/");
window.location.replace("https://www.kubera.co/");
}, 4500);
} else {
//console.log("BREACKPOINT 5");
if ((msgNotConnect != err.message) && idErrorFound == 0) {
	console.log("BREACKPOINT 6");
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	clearInterval(dataChannelInterval);
	exitFlag = false;
//Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR
if (myRol == "consultor") {
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
var JSONData = {
	'meetingId': meetingId,
	'status': "ERROR",
	'clientInMeeting': true,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO ANORMALMENTE");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
} else if (myRol == "cliente") {
	var intervalReportLostConnectionServer = setInterval(function() {
		clearInterval(intervalReportLostConnectionServer);
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
var JSONData = {
	'meetingId': meetingId,
	'status': "ERROR",
	'clientInMeeting': true,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO ANORMALMENTE");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
}, 5500);
}
}
var online = navigator.onLine;
//console.log("BREACKPOINT 7");
if (online && idErrorFound == 0) {
//console.log("BREACKPOINT 8");
if (err.message == "Lost connection to server.") {
//console.log("BREACKPOINT 9");
flagNotRedirectoOtherSide = false;
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
$('#text-conection').text(language.conexionServerLost);
swal({
	title: language.errTitle,
	text: language.conexionServerLost,
	timer: 6000,
	showConfirmButton: false
}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
var intervalRedirectTimer = 3500;
if (myRol == "cliente") {
	intervalRedirectTimer = intervalRedirectTimer + timeToReportSessionFinishedWhenLostServerConnection;
}
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, intervalRedirectTimer);
}
} else if (!online && idErrorFound == 0) {
//console.log("BREACKPOINT 10");
flagNotRedirectoOtherSide = false;
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
$('#text-conection').text(language.internetConnectionFailed);
swal({
	title: language.errTitle,
	text: language.internetConnectionFailed,
	timer: 6000,
	showConfirmButton: false
}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
}
}
if (!flagNotRedirectoOtherSide) {
	intervalToCloseWRTC = setInterval(function() {
		clearInterval(intervalToCloseWRTC);
		window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
	}, intervalRedirectTimer);
}
}
}
});
peer.on('call', function(mediaConnection) {
//console.log("NUEVA LLAMADA ENTRANTE");
mediaConnection.answer(videoStreaming);
mediaConnection.on('stream', function(stream) {
//console.log("CAPTURAMOS STREAMING ENTRANTE");
if (mediaConnection.metadata.type === 'Video') {
	videoInterval = setInterval(function(stream) {
//console.log("ESTADO DEL VIDEO -> " + stream.active);
if (stream.active) {
	sendPeerData('videoOk');
	$('#videoUsr2').prop('src', URL.createObjectURL(stream));
	videoStreamingOtherPeer = stream;
}
clearInterval(videoInterval);
if (monitoringIntervalFlagVideo == 0) {
	monitoringIntervalFlagVideo = 1;
	monitoringIntervalVideo = setInterval(monitoringMediaConnectionVideo, 2000);
}
}, 5000, stream);
} else if (mediaConnection.metadata.type === 'Audio') {
	audioInterval = setInterval(function(stream) {
//console.log("ESTADO DEL AUDIO -> " + stream.active);
if (stream.active) {
	sendPeerData('audioOk');
	$('#audioUsr2').prop('src', URL.createObjectURL(stream));
	audioStreamingOtherPeer = stream;
}
clearInterval(audioInterval);
if (monitoringIntervalFlagAudio == 0) {
	monitoringIntervalFlagAudio = 1;
	monitoringIntervalAudio = setInterval(monitoringMediaConnectionAudio, 2000);
}
}, 5000, stream);
} else if (mediaConnection.metadata.type === 'SharedScreen') {
	sharingScreenInterval = setInterval(function(stream) {
//console.log("ESTADO DEL VIDEO SHARING SCREEN -> " + stream.active);
if (stream.active) {
	sendPeerData('sharingScreenOk');
	var sharedScreenStreamOtherPeer = document.getElementById('sharingScreenVideoTag');
	sharedScreenStreamOtherPeer.src = window.URL.createObjectURL(stream);
	sharedScreenStreamOtherPeer.onloadedmetadata = function() {};
	videoSharingScreenStreamingOtherPeer = stream;
}
clearInterval(sharingScreenInterval);
if (monitoringIntervalFlagSharingScreen == 0) {
	monitoringIntervalFlagSharingScreen = 1;
	monitoringIntervalSharingScreen = setInterval(monitoringMediaConnectionVideoSharingScreen, 2000);
}
}, 5000, stream);
	$('#sharingScreen').hide();
}
});
mediaConnection.on('close', function() {
//console.log("YO O EL PEER REMOTO CERRAMOS LA CONEXION", mediaConnection.peer);
});
mediaConnection.on('error', function(err) {
//console.log("ERROR DECTADO EN EL MEDIA CONNECTION ", err);
});
});

function tryDataConnection() {
	dataConnectionOtherPeer = peer.connect(userOtherPeerId, {
		metadata: {
			username: 'metadata, nformacion a pasar a otra persona'
		}
	});
	dataConnectionManagement(dataConnectionOtherPeer);
}
peer.on('connection', function(dataConnection) {
//console.log("NUEVA CONEXION ENTRANTE CONMIGO");
dataConnectionManagement(dataConnection);
});

function dataConnectionManagement(dataConnection) {
	dataConnection.on('data', function(data) {
//console.log("DATOS RECIBIDOS DESDE EL OTRO PEER ", data);
if (data.action != undefined && data.action != null) {
	if (data.action == "docpaint") {
		if (data.tool == 'add') {
			if (data.num == 2015) {
				desk.addFigureToDoc(data);
			}
		} else if (data.tool == 'clear') {
			if (data.num == 2015) {
				desk.clearPap();
			}
		} else {
			if (data.num == 2015) {
				desk.delFigureFromDoc(data.id, data.num, "delete");
			}
		}
	}
	if (data.action == "updateWhiteBoard") {
//console.log("DESK OBJECT", desk);
if (desk != undefined && desk != null) {
	desk.refreshWhiteboard(data.msg);
} else {
	whiteBoardInterval = setInterval(function(data) {
		if (desk != undefined && desk != null) {
			desk.refreshWhiteboard(data);
			clearInterval(whiteBoardInterval);
		}
	}, 1000, data.msg);
}
}
if (data.action == "stopSharingScreen") {
	$('#sharingScreen').show();
	$('#sharingScreenVideoTag').prop('src', '');
}
if (data.action == 'slider') {
	switch (data.event) {
		case 'create':
		if (slider) {
			slider.destroy();
		}
		var config = JSON.parse(data.config);
		config.container = $('#slider-container');
		config.type = '';
		buildSlide(config);
		break;
		case 'next':
		slider.nextTo();
		break;
		case 'back':
		slider.backTo();
		break;
	}
} else if (data.action == "retreiveInformation") {
	var dataToSend = {
		action: "otherPeeerInformation",
		id: userLoguedId,
		name: myName,
		timeOnConference: conferenceDuration
	};
	sendPeerData(dataToSend);
} else if (data.action == "otherPeeerInformation") {
	$('#text-conection').text(language.connectedWith + data.name);
	var timeOnConferenceOtherPeer = data.timeOnConference;
	if (conferenceDuration > timeOnConferenceOtherPeer) {
		conferenceDuration = timeOnConferenceOtherPeer;
	}
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	swal.closeModal();
}
} else {
	if (data == "videoOk") {
		videoConnectInitFlag = 0;
	} else if (data == "audioOk") {
		audioConnectInitFlag = 0;
	} else if (data == "retryVideo") {
		triyingVideoConnectionFlag = 0;
		stopLocalVideo();
		videoConnectInitFlag = 0;
		$("#videoSharing").trigger('click');
	} else if (data == "retryAudio") {
		triyingAudioConnectionFlag = 0;
		stopLocalAudio();
		audioConnectInitFlag = 0;
		$("#audioSharing").trigger('click');
	} else if (data == 'videoOut') {
		triyingVideoConnectionFlag = 0;
		videoConnectInitFlag = 0;
		clearInterval(monitoringIntervalVideo);
		monitoringIntervalFlagVideo = 0;
		stopRemoteVideo();
		$('#videoUsr2').prop('src', '');
	} else if (data == 'audioOut') {
		triyingAudioConnectionFlag = 0;
		audioConnectInitFlag = 0;
		clearInterval(monitoringIntervalAudio);
		monitoringIntervalFlagAudio = 0;
		stopRemoteAudio();
	} else if (data == 'retryVideoSharingScreen') {
		triyingVideoSharingScreenConnectionFlag = 0;
		if (sharingScreenVideoTag != undefined || sharingScreenVideoTag != null) {
			sharingScreenVideoTag.getVideoTracks()[0].stop();
			$('#sharingScreenVideoTag').prop('src', '');
			$('#sharingScreen').text('Init Sharing Screen');
			sharingScreenVideoTag = undefined;
			onSharingScreen = false;
			var dataToSend = {
				action: "stopSharingScreen"
			};
			sendPeerData(dataToSend);
		}
		$("#sharingScreen").show();
		$("#sharingScreen").trigger('click');
	} else if (data == 'sessionFinished') {
		if (!sessionFinishedFlag) {
			exitFlag = false;
			sessionFinishedFlag = true;
// clearInterval(countdown);
//Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR
swal({
	title: language.tanksForUse,
	text: language.sesionTimeGone,
	timer: 6000,
	showConfirmButton: false
}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
}
} else if (data == 'openWhiteBoard') {
	if (!whiteBoardOpen) {
		$("#btnWhiteBoard").trigger('click');
	}
	if (slidesOpen) {
		$("#btnSlides").trigger('click');
	}
	if (sharingScreenOpen) {
		$("#btnShare").trigger('click');
	}
} else if (data == 'openSlides') {
	if (!slidesOpen) {
		$("#btnSlides").trigger('click');
	}
	if (whiteBoardOpen) {
		$("#btnWhiteBoard").trigger('click');
	}
	if (sharingScreenOpen) {
		$("#btnShare").trigger('click');
	}
} else if (data == 'openSharingScreen') {
	if (!sharingScreenOpen) {
		$("#btnShare").trigger('click');
	}
	if (whiteBoardOpen) {
		$("#btnWhiteBoard").trigger('click');
	}
	if (slidesOpen) {
		$("#btnSlides").trigger('click');
	}
}
}
});
dataConnection.on('open', function() {
//console.log("CONEXION CON OTRO PEER LISTA PARA UTILIZAR");
otherPeerConnection = true;
verifyWhiteBoardContent();
verifyMessagesPendient();
if (myRol == "consultor") {
	dataChannelInterval = setInterval(monitoringDataConnection, 2000);
}
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
sesionInit = true;
var dataToSend = {
	action: "retreiveInformation"
};
sendPeerData(dataToSend);
//Reportamos a kubera que LA SESION INICIO
var JSONData = {
	'meetingId': meetingId
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_inicia/';
//console.log("NOTIFICAR QUE LA SESION INICIO");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION INICIO
//Reportamos a Base de Datos interna que la sesion inicio
var JSONData = {
	'idMeeting': meetingId
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = urlKubera + "startMeeting";
//console.log("NOTIFICAR QUE LA SESION INICIO A BASE DE DATOS");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a Base de Datos interna que la seson inicio
//Iniciamos contador.
startConference();
});
dataConnection.on('close', function() {
//console.log("CONEXION CON OTRO PEER CERRADA");
otherPeerConnection = false;
stopLocalVideo();
stopRemoteVideo();
if (myRol == "consultor") {
	notificationLogic(preNotificationTime_client_12, postNotificationTime_client_12, 12);
	$('#text-conection').text(language.waitingClient);
} else if (myRol == "cliente") {
	notificationLogic(preNotificationTime_consultor, postNotificationTime_consultor, null);
	$('#text-conection').text(language.waitingConsultor);
}
});
dataConnection.on('error', function(err) {
//console.log("ERROR EN LA CONEXION CON OTRO PEER ", err);
otherPeerConnection = false;
stopLocalVideo();
stopRemoteVideo();
if (myRol == "consultor") {
	notificationLogic(preNotificationTime_client_12, postNotificationTime_client_12, 12);
	$('#text-conection').text(language.waitingClient);
} else if (myRol == "cliente") {
	notificationLogic(preNotificationTime_consultor, postNotificationTime_consultor, null);
	$('#text-conection').text(language.waitingConsultor);
}
});
}
$(document).on('click', '#btnLogout', function(event) {
	if (sesionInit == true) {
		swal({
			title: language.confirmGetOutPostnotificationTitle,
			text: language.confirmGetOutPostnotificationBody,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: language.confirmGetOutPostnotificationYes,
			cancelButtonText: language.confirmGetOutPostnotificationNo,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false,
			allowOutsideClick: false
		}).then(function() {
			clearInterval(preNotificationInterval);
			clearInterval(postNotificationInterval);
			clearInterval(dataChannelInterval);
			swal({
				title: language.yesClickedTitle,
				text: language.yesClickedBody,
				timer: 6000,
				showConfirmButton: false
			}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
//Reportamos a kubera que LA SESION TERMINO POR LOGOUT (SATISFACTORIAMENTE)
var JSONData = {
	'meetingId': meetingId,
	'status': "OK",
	'clientInMeeting': true,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO POR LOGOUT (SATISFACTORIAMENTE)");
ajaxSubmit(JSONData, url, 'post', responseSuccess, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO POR LOGOUT (SATISFACTORIAMENTE)
function responseSuccess(response) {
	if (response.status == "OK") {
//console.log("SESION TERMINADA Y REPORTADA EXITOSAMENTE A KUBERA");
//Reportamos a kubera que EL CONSULTOR VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR/CLIENTE VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR VA A SALIR
var intervalToCloseWRTC;
sendPeerData('sessionFinished');
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
} else {
	$("#btnLogout").trigger('click');
}
}
}, function(dismiss) {
// dismiss can be 'cancel', 'overlay',
// 'close', and 'timer'
if (dismiss === 'cancel') {
	swal({
		title: language.noClickedTitle2,
		text: "",
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
}
});
	}
});
$(document).on('click', '#videoSharing', function(event) {
	event.preventDefault();
	if (peerServerConnection == true && otherPeerConnection == true) {
//console.log("Iniciar compartir video");
videoConnectInitFlag = 1;
if (videoStatus) {
	stopLocalVideo();
	videoStatus = false;
	triyingVideoConnectionFlag = 0;
	videoConnectInitFlag = 0;
	sendPeerData('videoOut');
} else {
	retreiveLocalVideo(function(response) {
//console.log(response);
if (response.status == 'err') {
//console.log("ERROR AL BUSCAR STREAMING DE VIDEO ", response.errorMsg);
stopLocalVideo();
} else if (response.status == 'ok') {
//$('#videoSharing').text('Desactivate Cam');
$('#btnVideo').attr('src', '/img/icos/camOn.png');
$('#videoUsr1').prop('src', URL.createObjectURL(response.streaming));
videoCallOtherPeer(response.streaming);
}
});
}
} else {
//console.log("PEER SERVER CONNECTION IS UNAVALIABLE OR OTHER PEER CONNECTION IS UNAVALIABLE");
//console.log("INTENTANDO CONECATAR PEER");
tryDataConnection();
}
});
$(document).on('click', '#audioSharing', function(event) {
	event.preventDefault();
	if (peerServerConnection == true && otherPeerConnection == true) {
//console.log("Iniciar compartir audio");
audioConnectInitFlag = 1;
if (audioStatus) {
	stopLocalAudio();
	audioStatus = false;
	triyingAudioConnectionFlag = 0;
	audioConnectInitFlag = 0;
	sendPeerData('audioOut');
} else {
	retreiveLocalAudio(function(response) {
//console.log(response);
if (response.status == 'err') {
//console.log("ERROR AL BUSCAR STREAMING DE AUDIO ", response.errorMsg);
stopLocalAudio();
} else if (response.status == 'ok') {
//$('#audioSharing').text('Desactivate Mic');
$('#btnMicrophone').attr('src', '/img/icos/micOn.png');
audioCallOtherPeer(response.streaming);
}
});
}
} else {
//console.log("PEER SERVER CONNECTION IS UNAVALIABLE OR OTHER PEER CONNECTION IS UNAVALIABLE");
//console.log("INTENTANDO CONECATAR PEER");
tryDataConnection();
}
});

function retreiveLocalVideo(callback) {
	if (videoStreaming == undefined || videoStreaming == null) {
		var constraints = {
			video: {
				mandatory: {
					maxWidth: videoWidth,
					maxHeight: videoHeigth,
					minWidth: videoWidth,
					minHeight: videoHeigth
				}
			}
		};
		console.log("CONTRAINS DE VIDEO NUEVO --->> ", constraints);
		navigator.getUserMedia(constraints, function(localMediaStream) {
			videoStreaming = localMediaStream;
//console.log("STREAMING LOCAL", videoStreaming);
var result = [];
result['streaming'] = videoStreaming;
result['status'] = 'ok';
videoStatus = true;
callback(result);
}, function(err) {
	console.log("OCURRIO UN ERROR TOMANDO VIDEO LOCAL ", err);
	var result = [];
	result['status'] = 'err';
	result['errorMsg'] = err;
	videoStatus = false;
	callback(result);
	swal({
		title: language.errTitle,
		text: language.errTakingVideo,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
});
	} else {
		if (videoStreaming.open) {
			var result = [];
			result['streaming'] = videoStreaming;
			result['status'] = 'ok';
			videoStatus = true;
			callback(result);
		} else {
			var result = [];
			result['status'] = 'err';
			result['errorMsg'] = 'streaming inactive';
			videoStreaming.getTracks().forEach(track => track.stop());
			$('#videoUsr1').prop('src', '');
//$('#videoSharing').text('Activate Cam');
$('#btnVideo').attr('src', '/img/icos/camOff.png');
videoStreaming = undefined;
videoStatus = false;
callback(result);
}
}
}

function stopLocalVideo() {
//console.log("DETENIENDO VIDEO LOCAL");
if (videoStreaming != undefined || videoStreaming != null) {
	videoStreaming.getVideoTracks()[0].stop();
	$('#videoUsr1').prop('src', '');
//$('#videoSharing').text('Activate Cam');
$('#btnVideo').attr('src', '/img/icos/camOff.png');
videoStreaming = undefined;
videoStatus = false;
}
}

function stopRemoteVideo(retry) {
//console.log("DETENIENDO VIDEO COMPARTIDO");
if (videoMediaConnectionOtherPeer != undefined || videoMediaConnectionOtherPeer != null) {
	videoMediaConnectionOtherPeer = undefined;
	if (retry != undefined && retry == true && otherPeerConnection == true) {
//console.log("retry conection");
triyingVideoConnectionFlag = triyingVideoConnectionFlag + 1;
if (triyingVideoConnectionFlag >= 11) {
//console.log("IMPOSIBLE LOGRAR CONEXION DE VIDEO");
triyingVideoConnectionFlag = 0;
videoConnectInitFlag = 0;
} else {
	$("#videoSharing").trigger('click');
}
}
}
}
/////AUDIO FUNTIONS
function retreiveLocalAudio(callback) {
	if (audioStreaming == undefined || audioStreaming == null) {
		var constraints = {
			audio: true
		};
		navigator.getUserMedia(constraints, function(localMediaStream) {
			audioStreaming = localMediaStream;
//console.log("STREAMING LOCAL AUDIO ", audioStreaming);
var result = [];
result['streaming'] = audioStreaming;
result['status'] = 'ok';
audioStatus = true;
callback(result);
}, function(err) {
//console.log("OCURRIO UN ERROR TOMANDO AUDIO LOCAL " + err);
var result = [];
result['status'] = 'err';
result['errorMsg'] = err;
audioStatus = false;
callback(result);
});
	} else {
		if (audioStreaming.open) {
			var result = [];
			result['streaming'] = audioStreaming;
			result['status'] = 'ok';
			audioStatus = true;
			callback(result);
		} else {
			var result = [];
			result['status'] = 'err';
			result['errorMsg'] = 'streaming inactive';
			audioStreaming.getTracks().forEach(track => track.stop());
			$('#audioUsr1').prop('src', '');
//$('#audioSharing').text('Activate Mic');
$('#btnMicrophone').attr('src', '/img/icos/micOff.png');
audioStreaming = undefined;
audioStatus = false;
callback(result);
}
}
}

function stopLocalAudio() {
//console.log("DETENIENDO AUDIO LOCAL");
if (audioStreaming != undefined || audioStreaming != null) {
	audioStreaming.getAudioTracks()[0].stop();
//$('#audioSharing').text('Activate Mic');
$('#btnMicrophone').attr('src', '/img/icos/micOff.png');
audioStreaming = undefined;
audioStatus = false;
}
}

function stopRemoteAudio(retry) {
//console.log("DETENIENDO AUDIO COMPARTIDO");
if (audioMediaConnectionOtherPeer != undefined || audioMediaConnectionOtherPeer != null) {
	audioMediaConnectionOtherPeer = undefined;
	if (retry != undefined && retry == true && otherPeerConnection == true) {
//console.log("retry conection");
triyingAudioConnectionFlag = triyingAudioConnectionFlag + 1;
if (triyingAudioConnectionFlag >= 11) {
//console.log("IMPOSIBLE LOGRAR CONEXION DE AUDIO");
triyingAudioConnectionFlag = 0;
audioConnectInitFlag = 0;
} else {
	$("#audioSharing").trigger('click');
}
}
}
}

function videoCallOtherPeer(localMediaStream) {
//console.log("LLAMADA DE VIDEO <---></--->");
videoMediaConnectionOtherPeer = peer.call(userOtherPeerId, localMediaStream, {
	metadata: {
		type: 'Video'
	}
});
videoMediaConnectionOtherPeer.on('stream', function(stream) {
//console.log("call/ CAPTURAMOS STREAMING ENTRANTE");
});
videoMediaConnectionOtherPeer.on('close', function() {
//console.log("call/ YO O EL PEER REMOTO CERRAMOS LA CONEXION");
if (videoConnectInitFlag == 1) {
	stopRemoteVideo(true);
}
});
videoMediaConnectionOtherPeer.on('error', function(err) {
//console.log("call/ ERROR DETECTADO EN EL MEDIA CONNECTION ", err);
});
}

function audioCallOtherPeer(localMediaStream) {
//console.log("LLAMADA DE AUDIO <---></--->");
audioMediaConnectionOtherPeer = peer.call(userOtherPeerId, localMediaStream, {
	metadata: {
		type: 'Audio'
	}
});
audioMediaConnectionOtherPeer.on('stream', function(stream) {
//console.log("callAudio/ CAPTURAMOS STREAMING ENTRANTE");
});
audioMediaConnectionOtherPeer.on('close', function() {
//console.log("callAudio/ YO O EL PEER REMOTO CERRAMOS LA CONEXION", audioMediaConnectionOtherPeer.peer);
if (audioConnectInitFlag == 1) {
	stopRemoteAudio(true);
}
});
audioMediaConnectionOtherPeer.on('error', function(err) {
//console.log("callAudio/ ERROR DETECTADO EN EL MEDIA CONNECTION ", err);
});
}

function monitoringMediaConnectionVideo() {
	if (videoStreamingOtherPeer != undefined && videoStreamingOtherPeer != null) {
		if (!videoStreamingOtherPeer.active) {
//console.log("DETECTAMOS UN ERROR CON LA CONEXION DE VIDEO DEL OTRO PEER");
triyingVideoConnectionFlag = triyingVideoConnectionFlag + 1;
if (triyingVideoConnectionFlag >= 11) {
//console.log("IMPOSIBLE LOGRAR CONEXION DE VIDEO");
triyingVideoConnectionFlag = 0;
videoConnectInitFlag = 0;
clearInterval(monitoringIntervalVideo);
monitoringIntervalFlagVideo = 0;
} else {
//console.log("INTENTO DE RECONEXION DE VIDEO NUMERO " + triyingVideoConnectionFlag);
clearInterval(monitoringIntervalVideo);
sendPeerData('retryVideo');
monitoringIntervalFlagVideo = 0;
}
}
}
}

function monitoringMediaConnectionAudio() {
	if (audioStreamingOtherPeer != undefined && audioStreamingOtherPeer != null) {
		if (!audioStreamingOtherPeer.active) {
//console.log("DETECTAMOS UN ERROR CON LA CONEXION DE AUDIO DEL OTRO PEER");
triyingAudioConnectionFlag = triyingAudioConnectionFlag + 1;
if (triyingAudioConnectionFlag >= 11) {
//console.log("IMPOSIBLE LOGRAR CONEXION DE AUDIO");
triyingAudioConnectionFlag = 0;
audioConnectInitFlag = 0;
clearInterval(monitoringIntervalAudio);
monitoringIntervalFlagAudio = 0;
} else {
//console.log("INTENTO DE RECONEXION DE AUDIO NUMERO " + triyingAudioConnectionFlag);
clearInterval(monitoringIntervalAudio);
sendPeerData('retryAudio');
monitoringIntervalFlagAudio = 0;
}
}
}
}

function monitoringMediaConnectionVideoSharingScreen() {
	if (videoSharingScreenStreamingOtherPeer != undefined && videoSharingScreenStreamingOtherPeer != null) {
		if (!videoSharingScreenStreamingOtherPeer.active) {
//console.log("DETECTAMOS UN ERROR CON LA CONEXION DE VIDEO SHARING SCREEN DEL OTRO PEER");
triyingVideoSharingScreenConnectionFlag = triyingVideoSharingScreenConnectionFlag + 1;
if (triyingVideoSharingScreenConnectionFlag >= 11) {
//console.log("IMPOSIBLE LOGRAR CONEXION DE VIDEO SHARING SCREEN ");
triyingVideoSharingScreenConnectionFlag = 0;
clearInterval(monitoringIntervalSharingScreen);
monitoringIntervalFlagSharingScreen = 0;
} else {
//console.log("INTENTO DE RECONEXION DE SHARINGSCREEN NUMERO " + triyingVideoSharingScreenConnectionFlag);
clearInterval(monitoringIntervalSharingScreen);
sendPeerData('retryVideoSharingScreen');
monitoringIntervalFlagSharingScreen = 0;
}
}
}
}

function monitoringDataConnection() {
	if (!otherPeerConnection) {

		if(sesionInit){
			console.log("RECONNECTION TRIYING NUM: " + reloadDFlag);
			reloadDFlag = reloadDFlag - 1;
		}

		if(reloadDFlag == 0){
			location.reload();
		}
		tryDataConnection();
	}

}
tryDataConnection();
// QUALITY SELECT LOGIC
$(document).on('change', '#videoQuality', function(event) {
	var id = $(this).children(":selected").attr("id");
//console.log("QUALITY SELECTED --->>>" + id);
if (id == "QVGA") {
	videoWidth = 320;
	videoHeigth = 240;
	qualitySelected = "QVGA";
	swal.closeModal();
} else if (id == "VGA") {
	videoWidth = 640;
	videoHeigth = 480;
	qualitySelected = "VGA";
	swal.closeModal();
} else if (id == "HD") {
	videoWidth = 1280;
	videoHeigth = 720;
	qualitySelected = "HD";
	swal.closeModal();
}
if (videoStreaming != undefined || videoStreaming != null) {
	stopLocalVideo();
	videoStatus = false;
	triyingVideoConnectionFlag = 0;
	videoConnectInitFlag = 0;
	sendPeerData('videoOut');
	$("#videoSharing").trigger('click');
}
});

function verifyWhiteBoardContent() {
//console.log(desk.retreiveWhiteboardImage());
var pila = desk.retreiveWhiteboardImage();
if (pila.length > 0) {
//console.log("TENEMOS CONTENDIO EN WHITEBOARD PARA COMPARTIR", pila);
var dataToSend = {
	action: "updateWhiteBoard",
	msg: pila
};
sendPeerData(dataToSend);
} else {
//console.log("WHITEBOARD VACIO");
}
}

function buildSlide(config) {
//console.log("CONFIGURACION DE SLIDE PEER MANAGEMENT -->>", config);
slider = new Slider(config);
slider.paintMe();
}

function verifyMessagesPendient() {
	var arrayLength = dataChannelMessagePendient.length;
	for (var i = 0; i < arrayLength; i++) {
		sendPeerData(dataChannelMessagePendient[i], 1);
	}
}

function sendPeerData(data, retrySendmessage) {
	if (exitFlag) {
		var pointerSendMsg = 0;
		var retrySendmessageFlag = retrySendmessage;
		for (var currentPeerId in peer.connections) {
			if (!peer.connections.hasOwnProperty(currentPeerId)) {
				return;
			}
			var connectionsWithCurrentPeer = peer.connections[currentPeerId];
			for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
				if (connectionsWithCurrentPeer[i].type == 'data') {
					if (connectionsWithCurrentPeer[i].open) {
						connectionsWithCurrentPeer[i].send(data);
						pointerSendMsg = 1;
						if (retrySendmessageFlag == 1) {
							var index = dataChannelMessagePendient.indexOf(data);
							if (index > -1) {
								dataChannelMessagePendient.splice(index, 1);
							}
						}
					} else {
//console.log("CONEXION NOT AVALIABLE TO SEND INFO");
}
}
}
}
if (pointerSendMsg == 0) {
	tryDataConnection();
	if (retrySendmessageFlag != 1) {
		dataChannelMessagePendient.push(data);
	}
}
}
}
/*LOGICA DE ESPERA DE CONEXION DE 3 MINUTOS Y PREGUNTA POR RECONEXION.... */
function notificationLogic(timePreNotification, timePostNotification, selector) {
//selector es null si es el tiempo de un consultor
// es 8 si es tiempo de un cliente en area 8 del flujo (nunca se a iniciado la conversacion como tal)
// es 12 si es tiempo de un cliente en area 12 del flujo (ya se a iniciado la conversacion como tal,  pero el cliente perdio conexion)
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
preNotificationInterval = setInterval(function() {
//console.log("Selector " + selector);
if (myRol == "consultor") {
	if (selector == 8) {
		swal({
			title: "",
			text: language.waitTimeClient8,
			timer: 6000,
			showConfirmButton: false
		}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
	} else if (selector == 12) {
		swal({
			title: "",
			text: language.waitTimeClient12,
			timer: 6000,
			showConfirmButton: false
		}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
});
	}
} else if (myRol == "cliente") {
	swal({
		title: "",
		text: language.waitTimeConsultor,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
}
//Reportamos a kubera que el cliente/consultor no aparece
var JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/usuario_no_aparece/';
//console.log("NOTIFICAR QUE EL CLIENTE/CONSULTOR NO APARECE");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
tryDataConnection();
//Reportamos a kubera que el cliente/consultor no aparece
if (sesionInit) {
//Reportamos a kubera que EL OTRO PEER QUE PUEDE SER CLIENTE/CONSULTOR SALIO, PUEDE Q VUELVA O NO
var rolOtherUser = "cliente";
if (myRol == "cliente") {
	rolOtherUser = "consultor";
}
JSONData = {
	'meetingId': meetingId,
	'username': userOtherPeerId,
	'role': rolOtherUser
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL OTRO PEER QUE PUEDE SER CLIENTE/CONSULTOR SALIO, PUEDE Q VUELVA O NO");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL OTRO PEER QUE PUEDE SER CLIENTE/CONSULTOR SALIO, PUEDE Q VUELVA O NO
}
clearInterval(preNotificationInterval);
clearInterval(postNotificationInterval);
postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
}, timePreNotification);
}

function postNotificationLogic(selector, timePostNotification) {
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	if (!otherPeerConnection) {
//console.log("POS NOTIFICATION LOGIC");
if (myRol == "cliente") {
/////////////// CLIENTE ------------------
swal({
	title: language.confirmGetOutPostnotificationTitle2,
	text: language.confirmGetOutPostnotificationBody,
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: language.confirmGetOutPostnotificationYes,
	cancelButtonText: language.confirmGetOutPostnotificationNo,
	confirmButtonClass: 'btn btn-success',
	cancelButtonClass: 'btn btn-danger',
	buttonsStyling: false,
	allowOutsideClick: false
}).then(function() {
////TERMIAR FALLIDA O EXITOSA ?
swal({
	title: language.confirmGetOutPostnotificationTitle3,
	text: "",
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: language.finishSuccessful,
	cancelButtonText: language.finishFailed,
	confirmButtonClass: 'btn btn-success',
	cancelButtonClass: 'btn btn-danger',
	buttonsStyling: false,
	allowOutsideClick: false
}).then(function() {
//FINISH SUCCESSFULL
swal({
	title: language.confirmGetOutPostnotificationTitle,
	text: "",
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: language.yes,
	cancelButtonText: language.no,
	confirmButtonClass: 'btn btn-success',
	cancelButtonClass: 'btn btn-danger',
	buttonsStyling: false,
	allowOutsideClick: false
}).then(function() {
	swal({
		title: language.yesClickedTitle,
		text: language.yesClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	var clientInMeeting = false;
	if (selector != null && clientInMeeting == 8) {
		clientInMeeting = false;
	} else if (selector != null && clientInMeeting == 12) {
		clientInMeeting = true;
	}
	clearInterval(dataChannelInterval);
//Reportamos a kubera que LA SESION TERMINO SATISFACTORIAMENTE
var JSONData = {
	'meetingId': meetingId,
	'status': "OK",
	'clientInMeeting': clientInMeeting,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO SATISFACTORIAMENTE");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO SATISFACTORIAMENTE
//Reportamos a kubera que EL CONSULTOR VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR/CLIENTE VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR VA A SALIR
var intervalToCloseWRTC;
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
}, function(dismiss) {
// dismiss can be 'cancel', 'overlay',
// 'close', and 'timer'
if (dismiss === 'cancel') {
	swal({
		title: language.noClickedTitle,
		text: language.noClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
}
});
//FINISH SUCCESSFULL
}, function(dismiss) {
	if (dismiss === 'cancel') {
//FINISH FAILED
swal({
	title: language.confirmGetOutPostnotificationTitle,
	text: "",
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: language.yes,
	cancelButtonText: language.no,
	confirmButtonClass: 'btn btn-success',
	cancelButtonClass: 'btn btn-danger',
	buttonsStyling: false,
	allowOutsideClick: false
}).then(function() {
	swal({
		title: language.yesClickedTitle,
		text: language.yesClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	var clientInMeeting = false;
	if (selector != null && clientInMeeting == 8) {
		clientInMeeting = false;
	} else if (selector != null && clientInMeeting == 12) {
		clientInMeeting = true;
	}
	clearInterval(dataChannelInterval);
//Reportamos a kubera que LA SESION TERMINO CON FALLOS
var JSONData = {
	'meetingId': meetingId,
	'status': "FAIL",
	'clientInMeeting': clientInMeeting,
	'role': myRol
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO CON FALLOS");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO CON FALLOS
//Reportamos a kubera que EL CONSULTOR VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR/CLIENTE VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR VA A SALIR
var intervalToCloseWRTC;
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
}, function(dismiss) {
// dismiss can be 'cancel', 'overlay',
// 'close', and 'timer'
if (dismiss === 'cancel') {
	swal({
		title: language.noClickedTitle,
		text: language.noClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
}
});
//FINISH FAILED
}
});
////TERMIAR FALLIDA O EXITOSA ?
}, function(dismiss) {
	if (dismiss === 'cancel') {
		swal({
			title: language.noClickedTitle,
			text: language.noClickedBody,
			timer: 6000,
			showConfirmButton: false
		}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
		clearInterval(preNotificationInterval);
		clearInterval(postNotificationInterval);
		postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
	}
});
/////////////// CLIENTE ------------------
} else if (myRol == "consultor") {
/////////////// CONSULTOR -----------------
swal({
	title: language.confirmGetOutPostnotificationTitle2,
	text: language.confirmGetOutPostnotificationBody,
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: language.confirmGetOutPostnotificationYes,
	cancelButtonText: language.confirmGetOutPostnotificationNo,
	confirmButtonClass: 'btn btn-success',
	cancelButtonClass: 'btn btn-danger',
	buttonsStyling: false,
	allowOutsideClick: false
}).then(function() {
	swal({
		title: language.confirmGetOutPostnotificationTitle,
		text: "",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: language.yes,
		cancelButtonText: language.no,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		buttonsStyling: false,
		allowOutsideClick: false
	}).then(function() {
		swal({
			title: language.yesClickedTitle,
			text: language.yesClickedBody,
			timer: 6000,
			showConfirmButton: false
		}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
		var clientInMeeting = false;
		if (selector != null && clientInMeeting == 8) {
			clientInMeeting = false;
		} else if (selector != null && clientInMeeting == 12) {
			clientInMeeting = true;
		}
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
if (sesionInit) {
	clearInterval(dataChannelInterval);
//AQUI ENTRA CUANDO EL CONSULTOR DECIDE TERMINAR LA VIDEOCONFERENCIA
//PERO YA HABIA INICIADO LA VIDEOCONFERENCIA, ES DECIR, QUE LOGRO CONECTAR 
//ALGUNA VEZ CON EL CLIENTE
var JSONData = {
	'meetingId': meetingId,
	'status': "OK",
	'clientInMeeting': clientInMeeting,
	'notifyClient': true
}
} else {
	clearInterval(dataChannelInterval);
//AQUI ENTRA CUANDO EL CONSULTOR DECIDE TERMINAR LA VIDEOCONFERENCIA
//Y NUNCA SE INICIO LA VIDEOCONFERENCIA, ES DECIR, QUE NO LOGRO CONECTAR 
//ALGUNA VEZ CON EL CLIENTE
var JSONData = {
	'meetingId': meetingId,
	'status': "FAIL",
	'clientInMeeting': clientInMeeting,
	'role': myRol
}
}
var header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
var url = 'https://test.kubera.co/webrtc/sesion_termina/';
//console.log("NOTIFICAR QUE LA SESION TERMINO ANORMALMENTE");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que LA SESION TERMINO ANORMALMENTE
//Reportamos a kubera que EL CONSULTOR VA A SALIR
JSONData = {
	'meetingId': meetingId,
	'username': userLoguedId,
	'role': myRol
}
header = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'Authorization': 'A9b7766B1alcfr8n'
}
url = 'https://test.kubera.co/webrtc/usuario_desconectado/';
//console.log("NOTIFICAR QUE EL CONSULTOR/CLIENTE VA A SALIR");
ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
//Reportamos a kubera que EL CONSULTOR VA A SALIR
var intervalToCloseWRTC;
//console.log("URL TO REDIRECT -> " + redirectUrl + "&time=" + timeElapsedMinuts);
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
	window.location.replace(redirectUrl + "&time=" + timeElapsedMinuts);
}, 3500);
}, function(dismiss) {
// dismiss can be 'cancel', 'overlay',
// 'close', and 'timer'
if (dismiss === 'cancel') {
	swal({
		title: language.noClickedTitle,
		text: language.noClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
}
});
}, function(dismiss) {
// dismiss can be 'cancel', 'overlay',
// 'close', and 'timer'
if (dismiss === 'cancel') {
	tryDataConnection();
	swal({
		title: language.noClickedTitle,
		text: language.noClickedBody,
		timer: 6000,
		showConfirmButton: false
	}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
	clearInterval(preNotificationInterval);
	clearInterval(postNotificationInterval);
	postNotificationInterval = setInterval(postNotificationLogic, timePostNotification, selector, timePostNotification);
}
});
/////////////// CONSULTOR ------------------
}
}
}
var libCronometerControl = document.createElement("script");
libCronometerControl.type = "text/javascript";
libCronometerControl.src = urlKubera + "javascripts/libs-js/cronometerControl.js";
$("#necesaryScripts").append(libCronometerControl);

if(startClock == 1){

/*	var timePass = totalConferenceDuration - conferenceDuration;

	do {

		timePass = timePass - 1;
		timeElapsedSeconds = timeElapsedSeconds + 1;

		if(timeElapsedSeconds > 59){

			timeElapsedMinuts = timeElapsedMinuts + 1;
			timeElapsedSeconds = timeElapsedSeconds - 60;
		}

	}
	while (timePass < 60);
*/
	startConference();
}


} else {
//console.log("NAVEGADOR NO COMPATIBLE Y/O NAVEGADOR INCAPAZ DE TRANSMITIR AUDIO Y VDEO");
swal({
	title: language.errTitle,
	text: language.notSupportedBrowser,
	timer: 6000,
	showConfirmButton: false
}).then(function() {},
// handling the promise rejection
function(dismiss) {
	if (dismiss === 'timer') {
//console.log('I was closed by the timer')
}
})
intervalToCloseWRTC = setInterval(function() {
	clearInterval(intervalToCloseWRTC);
//console.log("URL TO REDIRECT -> https://www.kubera.co/");
window.location.replace("https://www.kubera.co/");
}, 4500);
}
});