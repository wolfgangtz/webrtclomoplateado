var isColckRunning = false;
var minTimeAlert = 300;

setTime();

function startConference() {

    if(!isColckRunning){

        isColckRunning = true;

        setTime();

        var countdown = setInterval(function() {
            conferenceDuration = conferenceDuration - 1;
            timeElapsedSeconds = timeElapsedSeconds + 1;

            if(timeElapsedSeconds > 59){

                timeElapsedMinuts = timeElapsedMinuts + 1;
                timeElapsedSeconds = timeElapsedSeconds - 60;
            }

            setTime();

            if (conferenceDuration < minTimeAlert){

                $('#text-conection').text(language.timeWIllFinish);
                $('#text-conection').css('color', '#F36C52');
            }

            if (conferenceDuration === 0) {

                clearInterval(countdown);
               // endSession(_config.meetingID, _config.idPeer, _config.otherUser, _config.rol, "OK");
               //SESION TERMINADA POR TIEMPO



                //Reportamos a kubera que LA SESION TERMINO POR QUE SE AGOTO EL TIEMPO

                var JSONData = {
                'meetingId': meetingId,
                'status': "OK"
                }
                var header = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'A9b7766B1alcfr8n'
                }
                var url = 'https://test.kubera.co/webrtc/sesion_termina/';

                console.log("NOTIFICAR QUE LA SESION TERMINO POR QUE SE AGOTO EL TIEMPO");

                ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');

                //Reportamos a kubera que LA SESION TERMINO POR QUE SE AGOTO EL TIEMPO



                //Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR

                JSONData = {
                'meetingId': meetingId,
                'username': myName,
                'role': myRol
                }
                header = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'A9b7766B1alcfr8n'
                }
                url = 'https://test.kubera.co/webrtc/usuario_desconectado/';

                console.log("NOTIFICAR QUE EL CONSULTOR VA A SALIR");

                ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');

                //Reportamos a kubera que EL CONSULTOR/CLIENTE VA A SALIR


swal({
            title: language.tanksForUse,
            text: language.sesionTimeGone,
            timer: 6000,
            showConfirmButton: false
}).then(
  function () {},
  // handling the promise rejection
  function (dismiss) {
    if (dismiss === 'timer') {
      console.log('I was closed by the timer')
    }
  }
);

                 intervalToCloseWRTC = setInterval(function(){
                  clearInterval(intervalToCloseWRTC);
                  window.location.replace(redirectUrl);
                }, 3500);

                 sessionFinishedFlag = true;
                 sendPeerData('sessionFinished');

            }
        }, 1000);
    }
}

            function sendPeerData(data,retrySendmessage) {

                var pointerSendMsg = 0;
                var retrySendmessageFlag = retrySendmessage;

                for (var currentPeerId in peer.connections) {
                    if (!peer.connections.hasOwnProperty(currentPeerId)) {
                        return;
                    }
                    var connectionsWithCurrentPeer = peer.connections[currentPeerId];
                    for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
                        if (connectionsWithCurrentPeer[i].type == 'data') {
                            if (connectionsWithCurrentPeer[i].open) {
                                connectionsWithCurrentPeer[i].send(data);
                                pointerSendMsg = 1;

                                if(retrySendmessageFlag == 1){
                                    var index = dataChannelMessagePendient.indexOf(data);
                                    if (index > -1) {
                                    dataChannelMessagePendient.splice(index, 1);
                                    }
                                }

                            } else {
                                console.log("CONEXION NOT AVALIABLE TO SEND INFO");
                            }
                        }
                    }
                }
                if (pointerSendMsg == 0) {
                    tryDataConnection();
                    if(retrySendmessageFlag != 1){
                        dataChannelMessagePendient.push(data);
                    }
                    
                }
            }

function numberToHourString(num){

    var str = "";

    if(num > 9){

        return String(num);
    }else{

        return "0" + num;
    }
}


function setTime() {

    var minutes = Math.floor(conferenceDuration / 60);
    var seconds = conferenceDuration % 60;

    $("#timer").html(numberToHourString(minutes) + ':' + numberToHourString(seconds));
}

function getElapsedMinutes(){

    return Math.floor(((totalConferenceDuration * 60) - conferenceDuration)/60);
}


