/**
* Funcion para hacer llamados AJAX
*
* @author   Sergio Martinez
* @param    Object          data            datos que seran enviados por AJAX
* @param    string          url             Url a la cual se hara la peticion AJAX
* @param    string          type            Tipo de paticion HTTP que sera realizada (GET, POST, PUT, DELETE)
* @param    function        successCallback funcion que se ejecutara si la peticion AJAX se ejecuta correctamente
* @param    function        errorCallback   fucnion que se ejecutara si la peticion AJAX falla
*/
function ajaxSubmit(data, url, type, successCallback, errorCallback, headers, dataType){
    $.ajax({
        data: data,
        url: url,
        type: type,
        success: successCallback,
        error: errorCallback,
        headers: headers,
        dataType: dataType
    });
}

function defaultErrorCallback(data){
	console.error(data);
}