function format12to24(hour, minutes, meridiane) {

	switch (meridiane){

		case "AM":

			if(hour == 12){

				hour = 0;
			}
			break;

		case "PM":

			hour += 12;
			break

		default:

			return "Error De Formato";
	}

	return toDigitNumber(hour) + ":" + toDigitNumber(minutes);		
}

function format24to12(hour, minutes) {

	var hour = hour >= hour -12 ? 12 : hour;
	var meridiane = hour  >= 12 ? "AM" : "PM";

	return toDigitNumber(hour) + ":" + toDigitNumber(minutes) + " " + meridiane;
}

function toDigitNumber(number){

	if(number >= 10){

		return number;
	}else{

		return "0" + number;
	}
}

function displayCurrentTime(format) {
	var date = new Date();
	var hours = date.getHours();
	var minutes = date.getMinutes();

	console.log(hours);
	console.log(minutes);

	switch (format){

		case '12':

			var time = format24to12(hours, minutes);
			break;

		case '24':

			var time = hours + ":" + minutes;
			break;

		default:

			return "Error De Formato";
	}

	return time;
}