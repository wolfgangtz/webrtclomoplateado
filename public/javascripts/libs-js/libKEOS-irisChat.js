function chat(user, pws, nick, room, chatContainerId, messageTextAreaId, sendButtonId){

	//Constatns
	var CONECTION_URL = 'https://webrtc01.kubera.co:3002/';
	var DEVICE = 'PC-DEVICE';
	var IS_ROOM = 1;
	var USER_EXTENCION = "@openfire.kubera.co";
	var CONFERENCE_EXTENCION = "@conference.openfire.kubera.co";
	var AUTH_KEY = "test";
	var OPENFIRE_URL = "https://openfire.kubera.co:9091/";
	var OPENFIRE_SECRET = "A9b7766B1alcfr8n";
	var FCM_KEY = 'none';
	var ENDPOINT_UPDATE_TPKEN_DEVIDE = 'updateTokenDevice';

	//Variables
	var userName = user;
	var password =  pws;
	var nickName = nick;
	var roomJID = room;
	var chat = $('#' + chatContainerId);
	var message = $('#' + messageTextAreaId);
	var socket; 
	var tokenComunicationWeb;
	var resourceLocal;

	console.log(sendButtonId);

	if(sendButtonId !== undefined && sendButtonId !== null){

		var sendButton = $("#" + sendButtonId);
	}

	this.connect = function() {

		socket = io.connect(CONECTION_URL);
		
		socket.on('getIdConnection', function(dataIn) {

			tokenComunicationWeb = socket.id;
			createConection();
		});

		socket.on('notifyCloseConversationRoom', function(data) {
			console.warn('alguien salio de una room donde usted esta -> ', data);
		});

		socket.on('receiverMessage', function(data) {

			chatNewMessages = chatNewMessages + 1;

			if(!chatOpen){

				var chatDiv = $('#img-btnChatting');
				$("#numMesagges").remove();
				chatDiv.append('<div id="numMesagges"><p>'+ chatNewMessages +'</p></div>');

				console.log("TIENES " + chatNewMessages + " NUEVOS MENSAJES DE CHAT" );
			}else{
			chatNewMessages = 0;
			}

			if (data.sendFor !== userName) {
			
				chat.append('<div class="col-xs-12 col-lg-12"> <p class="receptor">' + Autolinker.link(data.message) + '</p> </div>');
				chat.append('<div class="col-xs-12 col-lg-12"> <p class="hora_b">' + displayCurrentTime('12') + '</p> </div>');
			} else {
			
				chat.append('<div class="col-xs-12 col-lg-12"> <p class="emisor">' + Autolinker.link(data.message) + '</p> </div>');
				chat.append('<div class="col-xs-12 col-lg-12"> <p class="hora_a">' + displayCurrentTime('12') + '</p> </div>');
			}
			
			chat.scrollTop(chat[0].scrollHeight);

		
		});

		if(sendButton !== undefined){

			sendButton.click(function(e){

				e.preventDefault();
				sendChatMessage();
			});
		}

		message.on('keypress', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				
				sendChatMessage();
			}
		});
	}

	this.sendMessage = function(message){
		sendMessage(message);
	
	}

	/*this.setEmojis = function(emojis, emojiContainerId, optionalClasses){

		var cantEmojis = emojis.length;
		var emojiContainer = $("#" + emojiContainerId);
		var extraClass;

		if(optionalClasses == null or optionalClasses == undefined){

			extraClass =  "";
		}else

			extraClass = optionalClasses;
		}

		for (var a = 0; a < cantEmojis ; a++) {
			var html = 
		}
	}*/
		
	function createConection(){

		var send = {

			'origin': userName,
			'password': password,
			'nickName': nickName,
			'destination': roomJID,
			'device': DEVICE,
			'isRoom': IS_ROOM,
			'tokenComunicationWeb': tokenComunicationWeb,
			'userExtension': USER_EXTENCION,
			'conferenceExtension': CONFERENCE_EXTENCION,
			'authKey': AUTH_KEY,
			'urlOpenfire': OPENFIRE_URL,
			'secretKeyOpenfire': OPENFIRE_SECRET,
			'FCMKey': FCM_KEY
		};

		socket.emit('createConexion', send, function(data) {

			if (data) {

				resourceLocal = data.msg.myResource;

				var JSONData = {
					'username': userName,
					'resource': data.msg.myResource,
					'authKey': AUTH_KEY,
					'token': tokenComunicationWeb,
					'device': DEVICE 
				}

				var header = {
					
					'Content-Type': 'application/x-www-form-urlencoded'
				}

				var url = CONECTION_URL + ENDPOINT_UPDATE_TPKEN_DEVIDE;
				ajaxSubmit(JSONData, url, 'post', null, defaultErrorCallback, header, 'json');
			} else {

				console.error("Error de concexion de chat");
			}
		});
	}


	function sendMessage(text){

		var send = {
			'username': userName,
			'Destination_jid':roomJID,
			'nickName': nickName,
			'message': text,
			'resource': resourceLocal,
			'isRoom': IS_ROOM,
			'authKey': AUTH_KEY,
			'conferenceExtension': CONFERENCE_EXTENCION
		};

		socket.emit('sendMessage', send, function(data) {

			if (data) {
				if (data.msg == "msgEnviado") {
					message.val('');
				}
			}
		});
	}

	function sendChatMessage(){

		if (message.val() !== '') {

			sendMessage(message.val());
		}
	}

	function eliminarChat() {

		//console.log("eliminar conexion con " + resourceLocal + " " + _config.user_op);

	/*	var url = 'https://webrtc01.kubera.co:3002/deleteClientListener';

		var ArraData= {
				
				'username': userName,
				'resource': resourceLocal,
				'authKey': AUTH_KEY,
				'FCMKey': FCM_KEY
		}

		var headers = {
				'Content-Type': 'application/x-www-form-urlencoded'
		}

		ajaxSubmit(arrayData, url, 'post', null, defaultErrorCallback, header, 'json');		*/
	}
}