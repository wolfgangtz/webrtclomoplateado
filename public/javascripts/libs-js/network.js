
$(document).ready(function() {

console.log("LOGICA DE MONITOREO DE CALIDAD DE LA RED AGREGADA");



var imageAddr = urlKubera + "img/31120037-5mb.jpg"; 
var downloadSize = 4995374; //bytes
var showMessagePointer = 0;

function ShowProgressMessage(msg) {

    if (msg >= 0.01 && msg <= 0.5) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal1.png" title="Conexión" alt="Barras" est="Malo.">';
    } else if (msg > 0.5 && msg <= 1.0) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal2.png" title="Conexión" alt="Barras" est="Regular.">';
    } else if (msg > 1.0 && msg <= 3.0) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal3.png" title="Conexión" alt="Barras" est="Estable.">';
    } else if (msg > 3.0 && msg <= 4.0) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal4.png" title="Conexión" alt="Barras" est="Bueno.">';
    } else if (msg > 4.0 && msg <= 4.5) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal5.png" title="Conexión" alt="Barras" est="Excelente.">';
    } else if (msg > 4.5) {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal5.png" title="Conexión" alt="Barras" est="Excelente.">';
    } else {
        document.getElementById("conexion").innerHTML = '<img id="btnSignal" src="/img/icos/signal0.png" title="Conexión" alt="Barras" est="Sin señal.">';
    }

}

function InitiateSpeedDetection() {
    ShowProgressMessage("Loading the image, please wait...");
    window.setTimeout(MeasureConnectionSpeed, 1);
    window.setInterval(MeasureConnectionSpeed, 30000);
};    


function MeasureConnectionSpeed() {
    var startTime, endTime;
    var download = new Image();
    download.onload = function () {
        endTime = (new Date()).getTime();
        showResults();
    }
    
    download.onerror = function (err, msg) {
        ShowProgressMessage("Invalid image, or error downloading");
    }
    
    startTime = (new Date()).getTime();
    var cacheBuster = "?nnn=" + startTime;
    download.src = imageAddr + cacheBuster;
    
    function showResults() {
        var duration = (endTime - startTime) / 1000;
        var bitsLoaded = downloadSize * 8;
        var speedBps = (bitsLoaded / duration).toFixed(2);
        var speedKbps = (speedBps / 1024).toFixed(2);
        var speedMbps = (speedKbps / 1024).toFixed(2);
        showMessagePointer = showMessagePointer + 1;
        if(showMessagePointer == 10){
            showMessagePointer = 0;
            //mensaje se muestra cada 3 min.
            console.log("Your connection speed is:", 
            speedBps + " bps", 
            speedKbps + " kbps", 
            speedMbps + " Mbps");  
        }

        ShowProgressMessage(speedMbps);
    }
}

InitiateSpeedDetection();

});