/*** Var Globals ***/
var signal = '';


/*** Script Initial ***/
$(document).ready(function(){

	//Get attr conexion
	$('#btnSignal').click(function() {
		signal = $('#btnSignal').attr("est");
		console.log(signal);
		modalBandWidth(signal);
	});


	//Function Open Board
	$('#btnWhiteBoard').click(function() {
		
		if(!onSharingScreen && peerServerConnection == true && otherPeerConnection == true){
			modalWhiteBoard();
		}else{
			swal({
			title: language.errTitle,
			text: language.waitOtherPeer,
			timer: 6000,
			showConfirmButton: false
			}).then(
				function () {},
				// handling the promise rejection
				function (dismiss) {
					if (dismiss === 'timer') {
						console.log('I was closed by the timer')
					}
				})
		}
	});

	//Function Open Settings Globals
	$('#btnSettings').click(function() {
		modalSettings(qualitySelected);
	});

	//Function Open Information
	$('#btnInformation').click(function() {
		modalInformation();
	});

	//Function Open Slides
	$('#btnSlides').click(function() {
		if(!onSharingScreen && peerServerConnection == true && otherPeerConnection == true){
			openSlides();
		}else{

			swal({
			title: language.errTitle,
			text: language.waitOtherPeer,
			timer: 6000,
			showConfirmButton: false
			}).then(
				function () {},
				// handling the promise rejection
				function (dismiss) {
					if (dismiss === 'timer') {
						console.log('I was closed by the timer')
					}
				})
		}
	});

	//Function Open Share Screen
	$('#btnShare').click(function() {
		if(!onSharingScreen && peerServerConnection == true && otherPeerConnection == true){
			openShare();
		}else{

			swal({
			title: language.errTitle,
			text: language.waitOtherPeer,
			timer: 6000,
			showConfirmButton: false
			}).then(
				function () {},
				// handling the promise rejection
				function (dismiss) {
					if (dismiss === 'timer') {
						console.log('I was closed by the timer')
					}
				})
		}
	});


	//Function Open Chat
	$('#btnChatting').click(function() {
		modalChat();
	});

	//Funtion minimize Chat
	$('#minimize').click(function() {
		minimizeChat();
	});


});


function sendPeerData(data,retrySendmessage) {

	var pointerSendMsg = 0;
	var retrySendmessageFlag = retrySendmessage;

	for (var currentPeerId in peer.connections) {
		if (!peer.connections.hasOwnProperty(currentPeerId)) {
			return;
		}
		var connectionsWithCurrentPeer = peer.connections[currentPeerId];
		for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
			if (connectionsWithCurrentPeer[i].type == 'data') {
				if (connectionsWithCurrentPeer[i].open) {
					connectionsWithCurrentPeer[i].send(data);
					pointerSendMsg = 1;

					if(retrySendmessageFlag == 1){
						var index = dataChannelMessagePendient.indexOf(data);
						if (index > -1) {
						dataChannelMessagePendient.splice(index, 1);
						}
					}

				} else {
					console.log("CONEXION NOT AVALIABLE TO SEND INFO");
				}
			}
		}
	}
	if (pointerSendMsg == 0) {
		tryDataConnection();
		if(retrySendmessageFlag != 1){
			dataChannelMessagePendient.push(data);
		}
		
	}
}

function modalWhiteBoard() {

	$('#whiteboard-complete').toggle();

	var display = $('#whiteboard-complete').attr('style');
	if (display == 'display: none;') {
		whiteBoardOpen = false;
		$('#btnWhiteBoard').attr('src', '/img/icos/boardOff.png');
		$('#btnWhiteBoard').attr('title', 'Activar Pizarra');
	} else if (display == 'display: block;') {
		whiteBoardOpen = true;
		sendPeerData("openWhiteBoard");
		$('#btnWhiteBoard').attr('title', 'Desactivar Pizarra');
		$('#btnWhiteBoard').attr('src', '/img/icos/boardOn.png');
	}

}

function modalChat() {

	$('#chat-complete').toggle();

	var display = $('#chat-complete').attr('style');
	if (display == 'display: none;') {
		chatOpen = false;
		$('#btnChatting').attr('src', '/img/icos/chatOff.png');
		$('#btnChatting').attr('title', 'Activar Chat');
	} else if (display == 'display: block;') {
		chatOpen = true;
		chatNewMessages = 0;
		var chatDiv = $('#img-btnChatting');
		$("#numMesagges").remove();
		$('#btnChatting').attr('src', '/img/icos/chatOn.png');
		$('#btnChatting').attr('title', 'Desactivar Chat');
	}

}

function modalSettings(qualitySelected) {
var qvgaSelected = "";
var vgaSelected = "";
var hdSelected = "";

if(qualitySelected == "QVGA"){
qvgaSelected = "selected";
}else if(qualitySelected == "VGA"){
vgaSelected = "selected";
}else if(qualitySelected == "HD"){
hdSelected = "selected";
}else{

}

	swal({
	title: "Configurar resolución",
	type: 'info',
	html: '<div id="settings-video">\
			<select id="videoQuality">\
			<option value="QVGA" id="QVGA" ' + qvgaSelected + '>QVGA (320x240px)</option>\
			<option value="VGA" id="VGA" ' + vgaSelected + '>VGA (640x480px)</option>\
			<option value="HD" id="HD" ' + hdSelected + '>HD (1280x720px)</option>\
			</select>\
			</div>',
	showCloseButton: false,
	showCancelButton: false,
	showConfirmButton: false 
	})

}

function modalInformation() {

	swal({
	title: "Información",
	type: 'info',
	html: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus eros in odio maximus, condimentum pretium nisi.<br><br>\
			Aenean vestibulum vestibulum vestibulum. Vestibulum bibendum justo mi, nec fermentum risus facilisis eget. Duis sit amet lacus quis diam maximus malesuada. \
			Donec placerat molestie urna. Vivamus tortor nisl, sodales id arcu ut, ultrices semper tortor.<br><br> \
			Mauris libero mauris, volutpat vitae eleifend quis, placerat non diam. Quisque facilisis dolor non augue sodales, nec mollis nibh cursus. \
			In scelerisque metus ac diam rhoncus, id euismod felis aliquet. Vestibulum fringilla magna at ante egestas, id dictum enim egestas.",
	showCloseButton: true,
	showCancelButton: false
	})

}



function openSlides() {

	$('#slides-complete').toggle();

	var display = $('#slides-complete').attr('style');
	if (display == 'display: none;') {
		slidesOpen = false;
		$('#btnSlides').attr('title', 'Activar Slides');
		$('#btnSlides').attr('src', '/img/icos/slidesOff.png');
	} else if (display == 'display: block;') {
		slidesOpen = true;
		sendPeerData("openSlides");
		$('#btnSlides').attr('title', 'Desactivar Slides');
		$('#btnSlides').attr('src', '/img/icos/slidesOn.png');
	}

}


function openShare() {

	$('#share-complete').toggle();

	var display = $('#share-complete').attr('style');
	if (display == 'display: none;') {
		sharingScreenOpen = false;
		$('#btnShare').attr('title', 'Activar pantalla compartida');
		$('#btnShare').attr('src', '/img/icos/shareOff.png');
	} else if (display == 'display: block;') {
		sharingScreenOpen = true;
		sendPeerData("openSharingScreen");
		$('#btnShare').attr('title', 'Desactivar pantalla compartida');
		$('#btnShare').attr('src', '/img/icos/shareOn.png');
	}

}

function modalBandWidth(signal) {

	if (signal === 'Excelente.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal5.png' title='Conexión perfecta' /><br><br>Excelente.",
			showCloseButton: true,
			showCancelButton: false
		})

	}else if (signal === 'Bueno.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal4.png' title='Conexión perfecta' /><br><br>Bueno.",
			showCloseButton: true,
			showCancelButton: false
		})

	}else if (signal === 'Estable.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal3.png' title='Conexión perfecta' /><br><br>Estable.",
			showCloseButton: true,
			showCancelButton: false
		})
			
	}else if (signal === 'Regular.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal2.png' title='Conexión perfecta' /><br><br>Regular.",
			showCloseButton: true,
			showCancelButton: false
		})

	}else if (signal === 'Malo.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal1.png' title='Conexión perfecta' /><br><br>Malo.",
			showCloseButton: true,
			showCancelButton: false
		})

	}else if (signal === 'Sin señal.') {

		swal({
			title: "Ancho de Banda",
			type: 'info',
			html: "Tu Conexión es:<br/><br/><img id='bandWidth' src='/img/icos/signal0.png' title='Conexión perfecta' /><br><br>Sin señal",
			showCloseButton: true,
			showCancelButton: false
		})

	}

}

function minimizeChat() {

if(chatOpen)
{
	modalChat();
} 


}




