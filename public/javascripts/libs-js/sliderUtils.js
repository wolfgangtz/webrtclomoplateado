function Slider(config) {

   
    /** $ elements **/
    this.config = config;

    this.$body;
    this.$controls;
    this.$nextTo;
    this.$currentPage;
    this.$backTo;
    this.$container = config.container;

    this.numPages = config.pages;
    this.perPage = 1;
    this.$listElement = $('<div id="slider-elements" ></div>');
    this.curr = config.curr - 1;

    this.type = config.type;
    this.ext = config.ext;
    this.path = config.path;

    this.$listElement.children().css('display', 'none');
    this.$listElement.children().slice(0, this.perPage).css('display', 'block');

    var self = this;

    this.backTo = function() {
        var goToPage = this.curr - 1;
        if (goToPage >= 0) {
            this.goTo(goToPage);
        }
    };

    this.nextTo = function() {
        goToPage = this.curr + 1;
        if (goToPage < this.numPages) {
            this.goTo(goToPage);
        }
    };

    this.goTo = function(page) {
        var startAt = page * this.perPage,
            endOn = startAt + this.perPage;

        this.$listElement.children().css('display', 'none').slice(startAt, endOn).css('display', 'block');
        this.curr = page;
        this.$currentPage.html(this.curr + 1 + ' / ' + this.numItems);
    };

    //pinta el slider
    this.paintMe = function() {

        console.log("MOSTRAR SLIDES EN PANTALLA");
        this.$body = $('<div id="slider" ></div>');
        this.$controls = $('<div class="slider-controls"></div>');
        this.$nextTo = $('<a class="slider-nextTo">▶</a>');
        this.$currentPage = $('<span class="currentPage"></span>');
        this.$backTo = $('<a class="slider-backTo">◀</a>');

        if (self.type == 'mod') {
            this.$controls.append(this.$backTo);
            this.$controls.append(this.$currentPage);
            this.$controls.append(this.$nextTo);

            this.sendToPeer('create');

        } else {
            this.$controls.append(this.$currentPage);
        }

        this.$body.append(this.$listElement);
        this.$body.append(this.$controls);

        this.$container.append(this.$body);

        this.$nextTo.click(function() {

            self.nextTo();

            if (self.type == 'mod') {
                self.sendToPeer('next');
            }

        });

        this.$backTo.click(function() {

            self.backTo();

            if (self.type == 'mod') {
                self.sendToPeer('back');
            }

        });

        var pages = this.numPages;
        console.log("NUMERO DE PAGINAS A MOSTRAR ---> " + pages);

        for (var i = 0; i < pages; i++) {
            console.log("MOSTRAT IMAGEN -->" + '<img src="' + window.location.origin + this.path + '-' + i + '.' + this.ext + '"/ style="width: 100%; height: 100%">');
            this.$listElement.append('<img src="' + window.location.origin + this.path + '-' + i + '.' + this.ext + '"/ style="width: 100%; height: 100%">');
        }

        this.numItems = this.$listElement.children().length;
        this.$currentPage.html(this.curr + ' / ' + this.numItems);
        this.$listElement.children().css('display', 'none');
        this.$listElement.children().slice(0, this.perPage).css('display', 'block');

        this.goTo(this.curr);

    };

    this.destroy = function() {
        this.$body.remove();
    };

    this.sendToPeer = function(event) {

        var data = { action: "slider", event: event, };

        if (event == 'create') {
            data.config = JSON.stringify(this.config);
            data.config.curr = this.curr + 1;
        }

 


                    for (var currentPeerId in peer.connections) {
                if (!peer.connections.hasOwnProperty(currentPeerId)) {
                    return;
                }
                var connectionsWithCurrentPeer = peer.connections[currentPeerId];
                for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
                    if (connectionsWithCurrentPeer[i].type == 'data') {
                        if (connectionsWithCurrentPeer[i].open) {
                            connectionsWithCurrentPeer[i].send(data);
                        } else {
                            console.log("CONEXION NOT AVALIABLE TO SEND INFO");
                        }
                    }
                }
            }
    };



}


    var slidersLogic = document.createElement("script");
slidersLogic.type = "text/javascript";
slidersLogic.src = urlKubera + "javascripts/slidersLogic.js";
$("#necesaryScripts").append(slidersLogic);

