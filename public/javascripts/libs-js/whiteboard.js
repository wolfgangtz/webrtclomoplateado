/**
 * var p = {tool: p.tool, num: p.num, type: p.type, x: p.x, y: p.y, x2: p.x2, y2: p.y2, color: p.color, stroke_width: p.stroke_width, stroke_dasharray: p.stroke_dasharray, text: p.text, points: p.points, id: p.id};  
 * wrc.setColor($(this).data('color'));
 */
function confPaint(_elem) {
    this.color = '#000000';
    this.fontSize = 12;
    this.brushSize = 3;
    this.lineSize = 3;
    this.rectSize = 3;
    this.circSize = 3;
    this.rotation = 0;
    this.opasity = 1;
    this.stroke = "";
    this.fill = false;
    this.border = false;
    this.currSlide = 0;
    this.paintFg = [];
    var els2 = [];
    var els = [];
    var globalArray = [];
    glId = 1;
    this.paper = null;
    this.jtool = '';
    this.canvas = null;
    //this.r = null;
    this.coeff = 1;
    var that = this;
    this.initPaint = function(_w, _h, _canvas) {
        this.canvas = _canvas;
        this.canvas.style.width = _w + 'px';
        this.canvas.style.height = _h + 'px';
        this.canvas.style.display = 'block';
        this.canvas.style.position = 'relative';
        // this.canvas.style.border = '2px solid black';
        this.paper = new Raphael(this.canvas, _w, _h);
        this.addEventTools();
    };
    this.addEventTools = function() {
        $('#iBrush, #dBrush').click(function() {
            that.jtool = 'brush';
            that.iBrush();
            that.activeTool('brush');
        });
        $('#iLine, #dLine').click(function() {
            that.jtool = 'line';
            that.iLine();
            that.activeTool('line');
        });
        $('#iErase, #dErase').click(function() {
            that.jtool = 'erase';
            that.iErase();
            that.activeTool('erase');
        });
        $('#dText').click(function() {
            that.jtool = 'text';
            that.iText($('#dText_text'));
            // that.iText();
            that.activeTool('text');
        });
        $('#iRect, #dRect').click(function() {
            that.jtool = 'rectangle';
            that.iRectangle();
            that.activeTool('rectangle');
        });
        $('#iCircle, #dEllipse').click(function() {
            that.jtool = 'circle';
            that.iCircle();
            that.activeTool('circle');
        });
        $('#iChooseColor, #dChooseColor').click(function() {
            that.jtool = 'csColor';
            that.activeTool('csColor');
        });
        $('#iCancel, #dCancel').click(function() {
            that.iCancel();
            that.activeTool('cancel');
        });
        $('#iBack, #dBack').click(function() {
            that.iCancel();
            that.activeTool('cancel');
        });
        $('#iClear, #dClear').click(function() {
            that.clearPaint();
        });
    };
    this.sendPeer = function(data) {
        for (var currentPeerId in peer.connections) {
            if (!peer.connections.hasOwnProperty(currentPeerId)) {
                return;
            }
            var connectionsWithCurrentPeer = peer.connections[currentPeerId];
            for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
                if (connectionsWithCurrentPeer[i].type == 'data') {
                    if (connectionsWithCurrentPeer[i].open) {
                        connectionsWithCurrentPeer[i].send(data);
                    } else {
                        console.log("CONEXION NOT AVALIABLE TO SEND INFO");
                    }
                }
            }
        }
    };
    /** AGREGA UN ELEMENTO AL CANVAS ORGANIZADO DEPENDIENDO DEL TIPO DE ELEMENTO, ESTA ES LLAMADA POR SOCKET IO AL RECIBIR UN EMIT DEL SERVER */
    this.addFigureToDoc = function(p) {
        globalArray.push(p);
        if (typeof that.paintFg[p.num] == "undefined") {
            that.paintFg[p.num] = '';
        }
        if (!!p.type) {
            that.paintFg[p.num] += p.type + '&' + p.x + '&' + p.y + '&' + p.x2 + '&' + p.y2 + '&' + p.color + '&' + p.stroke_width + '&' + p.stroke_dasharray + '&' + p.text + '&' + p.points + '&' + p.id + ';';
        }
        if (that.paintFg[p.num]) {
            var subArr = that.paintFg[p.num].split(';');
            that.paper.clear();
            for (var i = 0; i < subArr.length - 1; i++) {
                var param = subArr[i].split('&');
                glId = parseInt(param[10]);
                switch (param[0]) {
                    case 'line':
                        var l = that.paper.path('M' + param[1] + ' ' + param[2] + 'L' + param[3] + ' ' + param[4]);
                        l.attr({
                            stroke: param[5],
                            'stroke-width': param[6],
                            'stroke-dasharray': param[7]
                        });
                        l.id = glId;
                        els2[els2.length] = l;
                        break;
                    case 'brush':
                        var l = that.paper.path(param[9]);
                        l.attr({
                            stroke: param[5],
                            'stroke-fill': param[5],
                            'stroke-width': param[6],
                            'stroke-dasharray': param[7]
                        });
                        l.id = glId;
                        els2[els2.length] = l;
                        break;
                    case 'rect':
                        var l = that.paper.rect(param[3], param[4], param[1], param[2]);
                        l.attr({
                            stroke: param[5],
                            'stroke-width': param[6],
                            'stroke-dasharray': param[7]
                        });
                        l.id = glId;
                        els2[els2.length] = l;
                        break;
                    case 'circ':
                        var l = that.paper.ellipse(param[3], param[4], param[1], param[2]);
                        l.attr({
                            stroke: param[5],
                            'stroke-width': param[6],
                            'stroke-dasharray': param[7]
                        });
                        l.id = glId;
                        els2[els2.length] = l;
                        break;
                    case 'text':
                        var l = that.paper.text(param[1], param[2], param[8]);
                        l.attr({
                            fill: param[5],
                            'font-weight': 'bolder',
                            'font-family': 'Arial',
                            'font-size': param[6]
                        });
                        l.id = glId;
                        els2[els2.length] = l;
                        break;
                }
            }
        }
    };
    /** BORRA UN ELEMENTO DEL CANVAS */
    this.delFigureFromDoc = function(id, num, obligatory) {
        console.log("eliminar en --> " + id + " num: " + num);
        var deleteAutorization = 0;
        if (obligatory != undefined && obligatory != null) {
            deleteAutorization = 1;
            for (var i in globalArray) {
                if (globalArray[i].num == num && globalArray[i].id == id) {
                    console.log(globalArray[i]);
                    globalArray.splice(i, 1);
                    break;
                }
            }
        } else {
            for (var i in globalArray) {
                if (globalArray[i].num == num && globalArray[i].id == id && globalArray[i].drawOwner == userLoguedId) {
                    console.log(globalArray[i]);
                    globalArray.splice(i, 1);
                    deleteAutorization = 1;
                    break;
                }
            }
        }
        if (deleteAutorization == 1) {
            var subArr = that.paintFg[num].split(';');
            els2 = [];
            that.paintFg = [];
            for (var j = 0; j < subArr.length; j++) {
                if (~subArr[j].indexOf(id)) {
                    subArr.splice(j, 0);
                } else {
                    if (typeof that.paintFg[num] == "undefined") {
                        that.paintFg[num] = '';
                    }
                    that.paintFg[num] += (subArr[j] + ';');
                }
            }
            var o = {};
            o.num = num;
            that.addFigureToDoc(o);
        } else {
            console.log("NO SE PUEDE ELIMINAR CONTENIDO, PUES NO HAY TRAZOS SUYOS PINTADOS");
        }
    };
    this.clearPaint = function() {
        that.clearPap();
        var X = {
            drawOwner: userLoguedId,
            action: "docpaint",
            tool: "clear",
            num: that.currSlide
        };
        that.sendPeer(X);
    };
    this.clearPap = function() {
        that.paper.clear();
        this.paintFg = [];
        els2 = [];
    };
    // Empezar a pintar figuras
    this.start_draw = function(p) {
        var o = {
            type: p
        };
        $(_elem).trigger('onstartdraw', [o]);
    };
    //Fin de la figura del dibujo
    this.stop_draw = function(p) {
        var o = {
            type: p
        };
        $(_elem).trigger('onstopdraw', [o]);
    };
    this.setColor = function(p) {
        this.color = p;
    };
    // Establecer el tamaño del texto
    this.setFontSize = function(_szFont) {
        that.fontSize = _szFont;
    };
    this.activeTool = function(p) {
        var o = {
            type: p
        };
        $(_elem).trigger('oncngtool', [o]);
    };
    this.iCancel = function() {
        if (globalArray.length === 0) return null;
        var pointer = globalArray.length - 1;
        for (var i in globalArray) {
            console.log("averiguando en ", globalArray[pointer - i]);
            if (globalArray[pointer - i].drawOwner == userLoguedId) {
                console.log("ELIMINAR --->>", globalArray[pointer - i]);
                var X = {
                    drawOwner: userLoguedId,
                    action: "docpaint",
                    num: that.currSlide,
                    tool: "del",
                    id: globalArray[pointer - i].id
                };
                that.sendPeer(X);
                that.delFigureFromDoc(globalArray[pointer - i].id, that.currSlide);
                break;
            }
        }
        // wrc_socket.emit('cmd', { cmd: '["actions","slides",{"action":"docpaint","num":"' + that.currSlide + '","tool":"del","id":"' + els2[num].id + '"}]' });
    };
    // Pincel
    this.iBrush = function() {
        var x2;
        var y2;
        var x;
        var y;
        var alli = '';
        var l;
        var H;
        var W;
        this.canvas.onmousedown = function(e) {
            that.getRandomInt();
            if (that.jtool != 'brush') return false;
            that.start_draw('brush');
            x2 = false;
            y2 = false;
            l = that.paper.set();
            els[els.length] = l;
            that.canvas.onmousemove = function(e) {
                e.preventDefault();
                if (!e) e = window.event;
                //  console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
                if (util.browser === 'Firefox') {
                    x = (e.layerX) * that.coeff;
                    y = (e.layerY) * that.coeff;
                } else {
                    x = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                    y = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
                }
                if (alli === '') alli += 'M' + x + ' ' + y;
                if (x2 !== false) {
                    l.push(that.paper.path('M' + x + ' ' + y + 'L' + x2 + ' ' + y2));
                    alli += 'L' + x2 + ' ' + y2;
                    l.attr({
                        stroke: that.color,
                        'stroke-width': that.brushSize,
                        'stroke-dasharray': that.stroke
                    });
                }
                x2 = x;
                y2 = y;
            };
        };
        this.canvas.onmouseup = function() {
            that.canvas.onmousemove = null;
            l = els[els.length - 1];
            l.remove();
            var l = that.paper.path(alli);
            if (that.fill) {
                l.attr({
                    'fill-opacity': that.opasity,
                    'fill': that.color
                });
                if (that.border) l.attr({
                    'stroke-width': that.brushSize,
                    stroke: that.color,
                    'stroke-dasharray': that.stroke
                });
                else l.attr({
                    'stroke-width': 0,
                    'stroke-opacity': that.opasity
                });
            } else {
                l.attr({
                    stroke: that.color,
                    'stroke-fill': that.color,
                    'stroke-width': that.brushSize,
                    'stroke-dasharray': that.stroke
                });
            }
            els[els.length - 1] = l;
            var X = {
                drawOwner: userLoguedId,
                action: "docpaint",
                num: that.currSlide,
                tool: "add",
                id: glId,
                type: "brush",
                x: x,
                y: y,
                x2: x2,
                y2: y2,
                color: that.color,
                stroke_width: that.brushSize,
                stroke_dasharray: that.stroke,
                text: "",
                points: alli
            };
            that.sendPeer(X);
            alli = '';
            l.remove();
            that.stop_draw('brush');
            that.addFigureToDoc(X);
        };
    };
    this.iLine = function() {
        var l;
        var x;
        var y;
        var x2;
        var y2;
        this.canvas.onmousedown = function(e) {
            that.getRandomInt();
            if (that.jtool != 'line') return false;
            if (!e) e = window.event;
            //console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
            if (util.browser === 'Firefox') {
                x = (e.layerX) * that.coeff;
                y = (e.layerY) * that.coeff;
            } else {
                x = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                y = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
            }
            that.start_draw('line');
            var lastl = false;
            var tsnum = els.length;
            that.canvas.onmousemove = function(e) {
                if (!e) e = window.event;
                //console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
                if (util.browser === 'Firefox') {
                    x2 = (e.layerX) * that.coeff;
                    y2 = (e.layerY) * that.coeff;
                } else {
                    x2 = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                    y2 = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
                }
                if (lastl) lastl.remove();
                l = that.paper.path('M' + x + ' ' + y + 'L' + x2 + ' ' + y2);
                l.attr({
                    stroke: that.color,
                    'stroke-width': that.lineSize,
                    'stroke-dasharray': that.stroke
                });
                lastl = l;
                els[tsnum] = l;
            };
        };
        this.canvas.onmouseup = function() {
            that.canvas.onmousemove = null;
            that.stop_draw('line');
            var X = {
                drawOwner: userLoguedId,
                action: "docpaint",
                num: that.currSlide,
                tool: "add",
                id: glId,
                type: "line",
                x: x,
                y: y,
                x2: x2,
                y2: y2,
                color: that.color,
                stroke_width: that.lineSize,
                stroke_dasharray: that.stroke,
                text: "",
                points: ""
            };
            that.sendPeer(X);
            l.remove();
            that.addFigureToDoc(X);
        };
    };
    //rectángulo
    this.iRectangle = function() {
        var l;
        var x;
        var y;
        var x2;
        var y2;
        var w;
        var h;
        var cx;
        var cy;
        this.canvas.onmousedown = function(e) {
            that.getRandomInt();
            if (that.jtool != 'rectangle') return false;
            //dow = false;
            if (!e) e = window.event;
            //   console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
            if (util.browser === 'Firefox') {
                x = (e.layerX) * that.coeff;
                y = (e.layerY) * that.coeff;
            } else {
                x = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                y = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
            }
            that.start_draw('rectangle');
            lastl = false;
            tsnum = els.length;
            that.canvas.onmousemove = function(e) {
                //dow = true;
                if (!e) e = window.event;
                //console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
                if (util.browser === 'Firefox') {
                    x2 = (e.layerX) * that.coeff;
                    y2 = (e.layerY) * that.coeff;
                } else {
                    x2 = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                    y2 = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
                }
                w = (Math.max(x, x2) - Math.min(x, x2));
                h = (Math.max(y, y2) - Math.min(y, y2));
                cx = Math.min(x, x2);
                cy = Math.min(y, y2);
                if (lastl) lastl.remove();
                l = that.paper.rect(cx, cy, w, h);
                if (that.fill) {
                    l.attr({
                        'fill-opacity': that.opasity,
                        'fill': that.color
                    });
                    if (that.border) l.attr({
                        'stroke-width': that.rectSize,
                        'rotation': that.rotation,
                        stroke: that.color,
                        'stroke-dasharray': that.stroke
                    });
                    else l.attr({
                        'stroke-width': 0,
                        'rotation': that.rotation,
                        'stroke-opacity': that.opasity
                    });
                } else {
                    l.attr({
                        stroke: that.color,
                        'rotation': that.rotation,
                        'stroke-width': that.rectSize,
                        'stroke-dasharray': that.stroke
                    });
                }
                lastl = l;
                els[tsnum] = l;
            };
        };
        this.canvas.onmouseup = function() {
            if (that.jtool != 'rectangle') return false;
            that.canvas.onmousemove = null;
            that.stop_draw('rectangle');
            var X = {
                drawOwner: userLoguedId,
                action: "docpaint",
                num: that.currSlide,
                tool: "add",
                id: glId,
                "type": "rect",
                x: w,
                y: h,
                x2: cx,
                y2: cy,
                color: that.color,
                stroke_width: that.rectSize,
                stroke_dasharray: that.stroke,
                text: "",
                points: "none"
            };
            that.sendPeer(X);
            l.remove();
            that.addFigureToDoc(X);
        };
    };
    this.iCircle = function() {
        var l;
        var x;
        var y;
        var x2;
        var y2;
        var w;
        var h;
        var cx;
        var cy;
        this.canvas.onmousedown = function(e) {
            that.getRandomInt();
            if (that.jtool != 'circle') return false;
            //dow = false;
            if (!e) e = window.event;
            //console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
            if (util.browser === 'Firefox') {
                x = (e.layerX) * that.coeff;
                y = (e.layerY) * that.coeff;
            } else {
                x = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                y = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
            }
            that.start_draw('circle');
            lastl = false;
            tsnum = els.length;
            that.canvas.onmousemove = function(e) {
                //dow = true;
                if (!e) e = window.event;
                // console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY); 
                if (util.browser === 'Firefox') {
                    x2 = (e.layerX) * that.coeff;
                    y2 = (e.layerY) * that.coeff;
                } else {
                    x2 = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                    y2 = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
                }
                w = (Math.max(x, x2) - Math.min(x, x2)) / 2;
                h = (Math.max(y, y2) - Math.min(y, y2)) / 2;
                cx = Math.min(x, x2) + w;
                cy = Math.min(y, y2) + h;
                if (lastl) lastl.remove();
                l = that.paper.ellipse(cx, cy, w, h);
                if (that.fill) {
                    l.attr({
                        'fill-opacity': that.opasity,
                        'fill': color
                    });
                    if (that.border) l.attr({
                        'stroke-width': that.circSize,
                        'rotation': that.rotation,
                        stroke: that.color,
                        'stroke-dasharray': that.stroke
                    });
                    else l.attr({
                        'stroke-width': 0,
                        'rotation': that.rotation,
                        'stroke-opacity': that.opasity
                    });
                } else {
                    l.attr({
                        stroke: that.color,
                        'rotation': that.rotation,
                        'stroke-width': that.circSize,
                        'stroke-dasharray': that.stroke
                    });
                }
                lastl = l;
                els[tsnum] = l;
            };
        };
        this.canvas.onmouseup = function() {
            if (that.jtool != 'circle') return false;
            that.canvas.onmousemove = null;
            that.stop_draw('circle');
            that.getRandomInt();
            var X = {
                drawOwner: userLoguedId,
                action: "docpaint",
                num: that.currSlide,
                tool: "add",
                id: glId,
                type: "circ",
                x: w,
                y: h,
                x2: cx,
                y2: cy,
                color: that.color,
                stroke_width: that.circSize,
                stroke_dasharray: that.stroke,
                text: "",
                points: ""
            };
            that.sendPeer(X);
            l.remove();
            that.addFigureToDoc(X);
        };
    };
    //Texto
    this.iText = function(elem) {
        console.log(elem);
        tmpTextElem = elem;
        this.canvas.onmouseup = null;
        this.canvas.onclick = function(e) {
            if (that.jtool != 'text') return false;
            that.start_draw('text');
            if (!e) e = window.event;
            //console.log("OFFEST X " + e.offsetX  + " OFFSET Y " + e.offsetY);
            if (util.browser === 'Firefox') {
                x = (e.layerX) * that.coeff;
                y = (e.layerY) * that.coeff;
            } else {
                var x = (e.offsetX === undefined ? e.layerX : e.offsetX) * that.coeff;
                var y = (e.offsetY === undefined ? e.layerY : e.offsetY) * that.coeff;
            }
            that.getRandomInt();
            text = tmpTextElem.val();
            tmpTextElem.val('');
            if (!text) return false;
            var l = that.paper.text(x, y, text);
            l.attr({
                fill: that.color,
                'rotation': that.rotation,
                'font-weight': 'bolder',
                'font-family': 'Arial',
                'font-size': that.fontSize,
                'opacity': that.opasity
            });
            els[els.length] = l;
            var rotate = 0;
            that.getRandomInt();
            var X = {
                drawOwner: userLoguedId,
                action: "docpaint",
                num: that.currSlide,
                tool: "add",
                id: glId,
                type: "text",
                x: x,
                y: y,
                x2: "",
                y2: "",
                color: that.color,
                stroke_width: that.fontSize,
                stroke_dasharray: "",
                text: text,
                points: ""
            };
            that.sendPeer(X);
            l.remove();
            that.stop_draw('text');
            that.addFigureToDoc(X);
        };
    };
    //Borrar
    this.iErase = function() {
        this.canvas.onmouseup = null;
        this.canvas.onclick = function(e) {
            var x = e.pageX || e.x;
            var y = e.pageY || e.y;
            var tst = that.paper.getElementByPoint(x, y);
            if (tst) {
                var X = {
                    drawOwner: userLoguedId,
                    action: "docpaint",
                    num: that.currSlide,
                    tool: "del",
                    id: tst.id
                };
                that.sendPeer(X);
            }
        };
    };
    this.getRandomInt = function() {
        glId = Math.floor(Math.random() * (999999 - 9 + 1)) + 9;
    };
    this.refreshWhiteboard = function(array) {
        console.log("reteive --->>>", array);
        for (var i in array) {
            console.log("pasamos al doc", array[i]);
            that.addFigureToDoc(array[i]);
        }
    }
    this.retreiveWhiteboardImage = function() {
        return globalArray;
    }
}
var whiteboardLogic = document.createElement("script");
whiteboardLogic.type = "text/javascript";
whiteboardLogic.src = urlKubera + "javascripts/whiteBoardLogic.js";
$("#necesaryScripts").append(whiteboardLogic);