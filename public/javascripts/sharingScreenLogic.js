


$(document).ready(function() {


console.log("AGREGADA LOGICA DE SHARING SCREEN");

    $(document).on('click', '#sharingScreen', function(event) {
        event.preventDefault();

        if(!onSharingScreen && peerServerConnection == true && otherPeerConnection == true){

 	
        getScreenId(function(error, sourceId, screen_constraints) {

            if (error == 'not-installed') {
                if (util.browser === 'Chrome') {


swal({
  title: language.errTitle,
  type: 'info',
  html: language.sharingScreenComplementNeed + "<a href='https://chrome.google.com/webstore/detail/ajhifddimkapgcifgcodmmfdlknahffk' target='_blank' >" + language.here + "</a>",
  showCloseButton: true,
  showCancelButton: false
})



            intervalToCloseWRTC = setInterval(function(){
            clearInterval(intervalToCloseWRTC);
            window.open('https://chrome.google.com/webstore/detail/ajhifddimkapgcifgcodmmfdlknahffk', '_blank');

            }, 2000);

                }
                if (util.browser === 'Firefox') {

swal({
  title: language.errTitle,
  type: 'info',
  html: language.sharingScreenComplementNeed + "<a href='https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness' target='_blank' >" + language.here + "</a>",
  showCloseButton: true,
  showCancelButton: false
})


            intervalToCloseWRTC = setInterval(function(){
            clearInterval(intervalToCloseWRTC);

                   window.open('https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness', '_blank');
 
            }, 2000);

                }
                return;
            }

            if (error == 'permission-denied') {
                alert("Has denegado permisos al compartir escritorio!");
                return;
            }

            if(sharingScreenVideoTag == undefined || sharingScreenVideoTag == null ){

            navigator.getUserMedia(screen_constraints, function(stream) {
            	sharingScreenVideoTag = stream;
            	console.log("STREAMING DE VIDEO SHARING SCREEN",sharingScreenVideoTag);
                $('#sharingScreenVideoTag').prop('src', URL.createObjectURL(sharingScreenVideoTag));

                  callScreenDesk = peer.call(userOtherPeerId, sharingScreenVideoTag, {
                        metadata: {
                            type: 'SharedScreen'
                        }
                    });
                  	onSharingScreen = true;
					$('#sharingScreen').text('Stop Sharing Screen');
                    sharingScreenVideoTag.getVideoTracks()[0].onended = function () {
                    console.log("STOP SHARING SCREEN EVENT......  ");

                        var dataToSend = { action: "stopSharingScreen" };
                        sendPeerData(dataToSend);

                      $('#sharingScreenVideoTag').attr('src', '');

                    };

            }, function(error) {
                console.error("ERROR TOMANDO STREAMING DE VIDEO DE SHARING SCREEN",error);
            });
}else{
 $('#sharingScreenVideoTag').prop('src', URL.createObjectURL(sharingScreenVideoTag));	
}



        });

}else{

sharingScreenVideoTag.getVideoTracks()[0].stop();
$('#sharingScreenVideoTag').prop('src', '');
$('#sharingScreen').text('Init Sharing Screen');
sharingScreenVideoTag = undefined;
onSharingScreen = false;
                        var dataToSend = { action: "stopSharingScreen" };
                        sendPeerData(dataToSend);

}

    });




        function sendPeerData(data) {
            var pointerSendMsg = 0;
            for (var currentPeerId in peer.connections) {
                if (!peer.connections.hasOwnProperty(currentPeerId)) {
                    return;
                }
                var connectionsWithCurrentPeer = peer.connections[currentPeerId];
                for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
                    if (connectionsWithCurrentPeer[i].type == 'data') {
                        if (connectionsWithCurrentPeer[i].open) {
                            connectionsWithCurrentPeer[i].send(data);
                            pointerSendMsg = 1;
                        } else {
                            console.log("CONEXION NOT AVALIABLE TO SEND INFO");
                        }
                    }
                }
            }
            if (pointerSendMsg == 0) {
                tryDataConnection();
            }
        }


});