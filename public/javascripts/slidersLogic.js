

$(document).ready(function() {


console.log("SLIDERS LOGIC UPLOADED");


    $('.upload-btn').on('click', function() {
        $('#upload-input').click();
        $('.progress').show();
        $('.progress-bar').text('0%');
        $('.progress-bar').width('0%');
    });


    $('#upload-input').on('change', function() {




        var files = $(this).get(0).files;
        var notUpload = true;
        if (files.length > 0) {

            swal({
  title: language.waitLoadingSlide,
  text: '',
  imageUrl: urlKubera + 'img/logos/favicon.png',
  imageWidth: 200,
  imageHeight: 200,
  animation: false,
  showConfirmButton: false,
  showCancelButton: false,
  allowOutsideClick: false
});

swal.showLoading();
        
percentComplete = parseInt(0);
$('.progress-bar').text(percentComplete + '%');
$('.progress-bar').width(percentComplete + '%');
$('.progress').show();

            var formData = new FormData();

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                formData.append('uploads[]', file, file.name);
            }

            formData.append('id', userLoguedId);

            var fileExtension = ['pdf', 'ppt', 'pptx', 'docx', 'doc'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                //alert("Formato de archivo no valido!");

                swal({
                title: language.errTitle,
                text: language.noValidFormat,
                timer: 6000,
                showConfirmButton: false
                }).then(function() {},
                // handling the promise rejection
                function(dismiss) {
                if (dismiss === 'timer') {
                //console.log('I was closed by the timer')                
                swal.hideLoading();
                swal.closeModal();
                }
                });
                notUpload = false;

                return;
            }else{

           if(notUpload){

            $.ajax({
                // url: 'https://kuberawebrtc.co/upload',
                url: urlKubera + 'upload',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {

                    $('.progress').hide();

                    if (data.error) {

                if(data.msg == "badExtension"){

                                        swal({
                    title: language.errTitle,
                    text: language.noValidFormat,
                    timer: 6000,
                    showConfirmButton: false
                    }).then(function() {},
                    // handling the promise rejection
                    function(dismiss) {
                    if (dismiss === 'timer') {
                    //console.log('I was closed by the timer')     
                     swal.hideLoading();
                    swal.closeModal();
                    }
                    });

                }else if(data.msg == "archiveTooLarge"){

                                        swal({
                    title: language.errTitle,
                    text: language.archiveTooLarge,
                    timer: 6000,
                    showConfirmButton: false
                    }).then(function() {},
                    // handling the promise rejection
                    function(dismiss) {
                    if (dismiss === 'timer') {
                    //console.log('I was closed by the timer')     
                     swal.hideLoading();
                    swal.closeModal();
                    }
                    });

                }




                    }else{

                    if (slider) {
                        slider.destroy();
                    }

                    data.container = $('#slider-container');
                    data.type = 'mod';
                    data.curr = 1;
                    swal.hideLoading();
                    swal.closeModal();
                    buildSlide(data);   
                    
                    }


                },
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);

                            $('.progress-bar').text(percentComplete + '%');
                            $('.progress-bar').width(percentComplete + '%');

                            if (percentComplete === 100) {
                                $('.progress-bar').html('Done');
                            }
                        }
                    }, false);
                    return xhr;
                }
            });
             }


            }



        }
    });


   function buildSlide(config) {
   	 console.log("CONFIGURACION DE SLIDE SLIDER LOGIC -->>",config);
        slider = new Slider(config);
        slider.paintMe();
    }


    $(document).on('keyup', '.colL_Slides', function(event) {
        // 37 left arrow
        // 39 right arrow

        if (event.keyCode == 37) {
            $(".colL_Slides_head_btnPrew").trigger('click');
        }
        if (event.keyCode == 39) {
            $(".colL_Slides_head_btnNext").trigger('click');
        }

    });



});