var express = require('express');
var router = express.Router();
var pgp = require('pg-promise')(); // Coneccion a postgreSql
var i18n = require("i18n");
var formidable = require('formidable');
var converter = require('office-converter')(); // Convertir office a pdf
var pdfPageCount = require('pdf_page_count'); // Cantidad de paginas de un pdf
var PDFImage = require("pdf-image").PDFImage; // Leer paginas de pdf para volver imagen
var xml = require('xml');
var request = require('request'); // Peticiones fuera del api
var fs = require('fs');
var path = require('path');
// Base de datos
var cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
};
var db = pgp(cn);
// Inicializa las rutas de express para el API
var router = express.Router();
// middleware para todas las peticiones
router.use(function(req, res, next) {
    // Sitio web al que desea permitir la conexión
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Métodos de solicitud que desea permitir
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Encabezados de solicitud que desea permitir
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Establezca en true si necesita que el sitio web incluya cookies en las solicitudes enviadas a la API (por ejemplo, en caso de que utilice sesiones)
    res.setHeader('Access-Control-Allow-Credentials', false);
    next();
});
router.get('/language/:lang', function(req, res) {
    console.log(req.params.lang);
    i18n.setLocale(req, req.params.lang);
    var languageCatalog = JSON.stringify(res.getCatalog(req.params.lang));
    console.log(languageCatalog);
    res.render('language', {
        languageCatalog: languageCatalog,
        urlKubera: process.env.URL_KUBERA
    });
});
/*CREATE*/
/*CREATE*/
/*CREATE*/
router.route('/create').get(function(req, res) {
    console.log("create service");
    var data = {
        roomname: req.query.meetingID,
        naturalname: req.query.meetingID + '_natural',
        description: 'VideoConference',
        owner: 'admin@kubera.openfire.co',
        authKey: 'test',
        urlOpenfire: 'https://openfire.kubera.co:9091/',
        secretKeyOpenfire: 'A9b7766B1alcfr8n'
    };
    console.log(data);
    //OJO QUE EN EL LADO SERVIDOR TENEMOS QUE CAMBIAR LA URL A LOCALHOST !!!!!
    request({
        url: 'https://webrtc01.kubera.co:3002/createRoom',
        method: 'POST',
        rejectUnauthorized: false,
        headers: {
                 'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(data)
    }, function(error, response, body) {
        console.log("LLEGUE DE APICHAT");
        if (error) {
            res.send(error);
        } else {
            respuesta = JSON.parse(body);
            if (respuesta.status == "err") {
                var XML = [{
                    response: [{
                        returncode: 'FAIL'
                    }, {
                        meeting: [{
                            meetingID: req.query.meetingID
                        }, {
                            createTime: new Date().getTime()
                        }, {
                            attendeePW: req.query.attendeePW
                        }, {
                            moderatorPW: req.query.moderatorPW
                        }, {
                            hasBeenForciblyEnded: false
                        }, {
                            messageKey: respuesta.msg
                        }]
                    }]
                }];
                res.set('Content-Type', 'text/xml');
                res.send(xml(XML));
                // res.json({ status: 'FAIL', meetingID: req.query.meetingID, messageKey: '', message: respuesta.msg });
            }
            if (respuesta.status == "ok") {
                var insert = [
                    req.query.name,
                    req.query.meetingID,
                    req.query.attendeePW,
                    req.query.moderatorPW,
                    req.query.logoutURL,
                    req.query.duration
                ];
                console.log("Insert to db");
                db.none("insert into conference(name, meetingid, attendeepw, moderatorpw, logouturl, duration) values ($1, $2, $3, $4, $5, $6) ", insert).then(function() {
                    var XML = [{
                        response: [{
                            returncode: 'SUCCESS'
                        }, {
                            meeting: [{
                                meetingID: req.query.meetingID
                            }, {
                                createTime: new Date().getTime()
                            }, {
                                attendeePW: req.query.attendeePW
                            }, {
                                moderatorPW: req.query.moderatorPW
                            }, {
                                hasBeenForciblyEnded: false
                            }, {
                                messageKey: 'Meeting has been create'
                            }]
                        }]
                    }];
                    res.set('Content-Type', 'text/xml');
                    res.send(xml(XML));
                    // res.send({ status: 'SUCCESS', meetingID: req.query.meetingID });
                }).catch(function(error) {
                    console.log("FAIL DB INSERTION");
                    var XML = [{
                        response: [{
                            returncode: 'FAIL'
                        }, {
                            meeting: [{
                                meetingID: req.query.meetingID
                            }, {
                                createTime: new Date().getTime()
                            }, {
                                attendeePW: req.query.attendeePW
                            }, {
                                moderatorPW: req.query.moderatorPW
                            }, {
                                hasBeenForciblyEnded: false
                            }, {
                                messageKey: error.message
                            }]
                        }]
                    }];
                    res.set('Content-Type', 'text/xml');
                    res.send(xml(XML));
                    // res.json({ status: 'FAIL', meetingID: req.query.meetingID, messageKey: '', message: error.message });
                });
            }
        }
    });
});
/*CREATE*/
/*CREATE*/
/*CREATE*/

/*JOIN*/
/*JOIN*/
/*JOIN*/
router.route('/join').get(function(req, res) {
    var _rol, _room, _onready, _otherUser, idKubera, _redirectURL, _conferenceId;
    //_timeLeft = 0;
    db.any("select * from conference where meetingid=$1 and finish_at is null", [req.query.meetingID]).then(function(data) {
        if (data.length === 0) {
            // La videoconferencia no existe
            res.json({
                status: 'ERR',
                meetingID: req.query.meetingID,
                messageKey: '',
                message: 'Videoconferencia no existente'
            });
        } else {
            //console.log(data);
            _redirectURL = data[0].logouturl;
            var rol, password;
            // La videoconferencia si existe
            setRoom(data);
            password = req.query.password;
            // Verifica si la contraseña es la del attendee
            if (data[0].attendeepw == password) {
                _rol = 'attendee';
                _otherUser = data[0].moderatorpw.split("p")[0];
            } else if (data[0].moderatorpw == password) {
                _rol = 'moderator';
                _otherUser = data[0].attendeepw.split("p")[0];
            } else {
                _: rol = null;
                res.json({
                    status: 'ERR',
                    meetingID: req.query.meetingID,
                    messageKey: '',
                    message: 'Contrasena Incorrecta'
                });
            }
            idKubera = password.split("p")[0];
            _conferenceId = data[0].meetingid;
           
                var insertData = [
                    _conferenceId,
                    req.query.fullName,
                    _rol,
                    idKubera
                ];
                db.none("insert into conference_session(conference_id, username, rol, idkubera) values ($1, $2, $3, $4) ", insertData).then(function() {
                    idKubera = req.query.password.split('p')[0];
                    request({
                        url: 'https://openfire.kubera.co/api/users?idkubera=' + idKubera + '&schema=kubera',
                        method: 'GET',
                        headers: {
                            'Authorization': 'A9b7766B1alcfr8n'
                        }
                    }, function(error, response, body) {
                        if (error) {
                            res.send(error);
                        } else {
                            respuesta = JSON.parse(body);
                            var lng = req.query.languageJoin;
                            // Crea una sesion
                            var sess = req.session;
                            var data = {
                                meetingID: _room.meetingid,
                                idPeer: idKubera,
                                roomName: req.query.meetingID,
                                userName: respuesta.nickname,
                                rol: _rol,
                                user_op: respuesta.username,
                                pw_op: respuesta.password,
                                onready: _onready,
                                otherUser: _otherUser,
                                redirectURL: _redirectURL,
                                peer_url: process.env.PEER_URL,
                                peer_path: process.env.PEER_PATH,
                                peer_port: process.env.PEER_PORT,
                                peer_key: process.env.PEER_KEY,
                                url_kubera: process.env.URL_KUBERA,
                                language: lng,
                                timers: {}
                            };
                            // Guarda los datos de la sesion
                            sess.conference = data;
                            //res.redirect('/conference/meeting');
                           console.log(_room.meetingid);
                    request({
                        url: 'https://test.kubera.co/webrtc/webrtc_params/',
                        method: 'POST',
                        form: {
                            meetingID: _room.meetingid
                        },
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }, function(error, response, body) {
                        if (error) {
                            res.send(error);
                        } else {

                            respuesta = JSON.parse(body);

                            console.log("Respuesta",respuesta);

                            if(!respuesta.status == "OK"){
                                res.send("ERROR TOMANDO INFORMACION DE TIMERS");
                            }else{
                                // res.send(respuesta.msg);
                                sess.conference.timers = respuesta.msg; 
                                res.redirect('/meeting');
                            }

                           
                        }
                    });

                        }
                    });
                }).catch(function(error) {
                    res.json({
                        status: 'ERR',
                        meetingID: req.query.meetingID,
                        messageKey: '',
                        message: 'Errro al insertar sesion'
                    });
                });

        }
    }).catch(function(error) {
        // error
        res.json(error);
    });

    function setRoom(data) {
        _room = data[0];
    }
});
/*JOIN*/
/*JOIN*/
/*JOIN*/


/*MEETING*/
/*MEETING*/
/*MEETING*/
router.route('/meeting').get(function(req, res) {
    if (req.session.conference) {

        db.any("select duration, start_at from conference where meetingid=$1", [req.session.conference.meetingID]).then(function(data) {

            var diff = 0;

            console.log(data);

            if(data[0].start_at != null){

                var justNow = Date.now();
                var start = new Date(data[0].start_at);
                diff = parseInt((justNow - start.getTime())/1000);
            }


            req.session.conference.duration = (parseInt(data[0].duration)*60) - diff;

            var flagNegativeTime = 0;

            if(req.session.conference.duration <= 0){
                    flagNegativeTime = 1;
                    request({
                        url: 'https://test.kubera.co/webrtc/conference_finished_by_time/',
                        method: 'GET',
                        headers: {
                            'Authorization': 'A9b7766B1alcfr8n'
                        }
                    }, function(error, response, body) {
                        if (error) {
                            res.send(error);
                        } else {

                            respuesta = JSON.parse(body);

                            console.log("Respuesta tiempo negativo: ",respuesta);
                            console.log("Redirect to " + req.session.conference.redirectURL + '&time=65000');
                            res.redirect(req.session.conference.redirectURL + '&time=65000' );
                        }
                    });



            }

            req.session.conference.initialDuration = data[0].duration;
            console.log(req.session.conference.duration);

                 var configuracionVideoConference = JSON.stringify(req.session.conference);

                i18n.setLocale(req, req.session.conference.language);
                var languageCatalog = JSON.stringify(res.getCatalog(req.params.lang));
                //console.log(languageCatalog);

                if(flagNegativeTime == 0){
res.render('index', { config: configuracionVideoConference, languageCatalog: languageCatalog, urlKubera: process.env.URL_KUBERA });
                }
            

        }).catch(function(error) {
            res.json({
                status: 'fail',
                error: error
            });
        });




        
    } else {
        res.json({ status: 'FAIL', message: 'No tiene sesion' });
    }
});




router.route('/startMeeting').post(function(req, res){

    var idMeeting = req.body.idMeeting;
    db.none("update conference SET start_at = now() where meetingid = $1 and start_at is null", idMeeting).then(function() {
        console.log("Sesion iniciada");
    }).catch(function(error) {
        console.log(error);
            res.json({
        status: 'ERR'
    });
            return;
    });
    res.json({
        status: 'OK'
    });
});



router.route('/endMeeting').post(function(req, res) {

    var meetingId = req.body.meetingId;
    var estado = req.body.state;
    var clientUser = req.body.clientUser;
    var consultantUser = req.body.consultantUser;
    var User1;

    if (clientUser == null) {

        User1 = consultantUser;
    } else {

        User1 = clientUser
    }

    db.none("UPDATE conference SET finish_at=$1 WHERE meetingid=$2", ['now()', req.body.meetingID]).then(function() {
        console.log("Inserto la finalizacion de la conferencia" + req.body.meetingId);
    }).catch(function(error) {
        console.log("No inserto la finalizacion de la conferencia");
        //console.log(error);
    });

    res.json({
        status: 'ok'
    });
});


/*MEETING*/
/*MEETING*/
/*MEETING*/


router.route('/createXML').get(function(req, res) {
    var response = [{
        response: [{
            returncode: 'SUCCESS'
        }, {
            meeting: [
                { meetingID: 'test' },
                { createTime: new Date().getTime() },
                { attendeePW: 'ap' },
                { moderatorPW: 'mp' },
                { hasBeenForciblyEnded: false },
                { messageKey: 'Meeting has been create' }
            ]
        }]
    }];
    res.set('Content-Type', 'text/xml');
    res.send(xml(response));
});



router.route('/upload').post(function(req, res) {
    var directorioRaiz = __dirname;
    directorioRaiz = directorioRaiz.replace(/\/routes/g, '');
    console.log("CARGAR ARCHIVO ",directorioRaiz);
    // create an incoming form object
    var form = new formidable.IncomingForm();
    // Tamaño maximo
    form.maxFieldsSize = 10 * 1024 * 1024;
    // specify that we want to allow the user to upload multiple files in a single request
    form.multiples = false;
    // store all uploads in the /uploads directory
    form.uploadDir = path.join(directorioRaiz, '/public/uploads');
    // Captura los campos adicionales enviados ej: id
    form.on('field', function(name, value) {
        if (name == 'id') {
            Directory_Id = directorioRaiz + '/public/uploads/slides/' + value + '/';
            imgData = [];
            if (!fs.existsSync(Directory_Id)) {
                fs.mkdirSync(Directory_Id);
            }
        }
    });
    // every time a file has been uploaded successfully,
    // rename it to it's orignal name
    form.on('file', function(field, file) {
        var fileNameFile = file.name;
        fileNameFile = fileNameFile.replace(/ /g, '');
        console.log("NOMBRE DE ARCHIVO " + fileNameFile);
        var fileType = fileNameFile.split('.').pop();
        var fileExtension = ['pdf', 'ppt', 'pptx', 'docx', 'doc'];
        if (fileExtension.indexOf(fileType) >= 0) {
            console.log("Extension permitida");
        } else {
            fs.unlinkSync(file.path);
            res.send({
                error: true,
                msg: "badExtension"
            });
            return;
        }
        // Envia el archivo de temporal a la carpeta uploads
        fs.rename(file.path, path.join(form.uploadDir, fileNameFile));
        if (fileType == 'pdf') {
            generateSlide(path.join(form.uploadDir, fileNameFile));
        } else {
            // Genera el pdf de el archivo subido
            converter.generatePdf(path.join(form.uploadDir, fileNameFile), function(err, result) {
                // Si el archivo fue convertido a PDF correctamente
                if (result.status === 0) {
                    // Elimina el archivo subido de office
                    fs.unlink(path.join(form.uploadDir, fileNameFile));
                    // Guarda la url de el archivo pdf para eliminarlo posteriormente
                    File_Pdf = result.outputFile;
                    generateSlide(File_Pdf);
                }
            });
        }

        function generateSlide(File_Pdf) {
            // Saca el numero de paginas
            var counter = 0;
            pdfPageCount.count(File_Pdf, function(resp) {
                console.log("RESPUESTA DE pdfPageCount ", resp);
                var countPage = resp.data;


            if(countPage > 30){
            res.send({
                error: true,
                msg: "archiveTooLarge"
            });
            return;
                }else{
                var images = [];
                for (var i = 0; i < countPage; i++) {
                    // Lee el archivo pdf
                    var pdfImage = new PDFImage(File_Pdf);
                    // Lee pagina por pagina
                    pdfImage.convertPage(i).then(function(imagePath) {
                        var filename = imagePath.replace(/^.*[\\\/]/, '');
                        counter = counter + 1;
                        console.log("Listo " + filename + " i= " + i + " count: " + counter);
                        fs.rename(directorioRaiz + '/public/uploads/' + filename, Directory_Id + filename);
                        if(counter == countPage){
               setTimeout(function() {
                    var filename = File_Pdf.replace(/^.*[\\\/]/, '');
                    res.json({
                        'path': '/uploads/slides/' + req.session.conference.idPeer  + "/" + filename.replace('.pdf', ''),
                        'pages': countPage,
                        'ext': 'png'
                    });
                }, 2000);
                        }
                    });
                }

}


 
            }); // body...
        }
    });
    // log any errors that occur
    form.on('error', function(err) {
        //console.log('An error has occured: \n' + err);
    });
    // once all the files have been uploaded, send a response to the client
    form.on('end', function() {
        //res.send(imgData);
    });
    // parse the incoming request containing the form data
    form.parse(req);
});


router.get('/', function(req, res, next) {
 res.json({ message: 'Welcome to kubera webRTC service' });
//  res.render('index', { urlKubera: process.env.URL_KUBERA });
});
router.get('/web-rtc-status', function(req, res, next) {
    res.json({
        status: 'OK'
    });
});



router.route('/notifiyUsuarioNoConecta').post(function(req, res) {

});

module.exports = router;